﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DL.Entities
{
    public class BaseEntity
    {
        public Guid Id { get; set; }
    }
}
