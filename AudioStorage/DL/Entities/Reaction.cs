﻿using DL.Enums;
using System;

namespace DL.Entities
{
    public class Reaction : BaseEntity
    {
        public Guid UserId { get; set; }
        public Guid AudioId { get; set; }
        public Rate Rate { get; set; }

        public Audio Audio { get; set; }
        public User User { get; set; }
    }
}
