﻿using System;
using System.Collections.Generic;

namespace DL.Entities
{
    public class Audio : BaseEntity
    {
        public Guid GenreId { get; set; }
        public string Name { get; set; }
        public string AuthorName { get; set; }
        public DateTime PostedDate { get; set; }
        public Guid? UserId { get; set; }

        public Genre Genre { get; set; }
        public ICollection<Reaction> Reactions { get; set; }
        public User User { get; set; }
    }
}
