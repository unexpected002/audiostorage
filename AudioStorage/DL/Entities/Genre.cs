﻿using System;
using System.Collections.Generic;

namespace DL.Entities
{
    public class Genre : BaseEntity
    {
        public string Name { get; set; }
        public ICollection<Audio> Audio { get; set; }
    }
}
