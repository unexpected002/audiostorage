﻿using DL.Interfaces;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;

namespace DL.Entities
{
    public class User : IdentityUser<Guid>
    {
        public DateTime Birthdate { get; set; }

        public ICollection<Audio> Audio { get; set; }
        public ICollection<Reaction> Reactions { get; set; }
    }
}