﻿using DL.Data;
using DL.Entities;
using DL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DL.Repositories
{
    public class AudioRepository : GenericRepository<Audio>, IAudioRepository
    {
        private readonly ApplicationContext _db;
        public AudioRepository(ApplicationContext db) : base(db)
        {
            this._db = db;
        }
        public async Task<IEnumerable<Audio>> GetAllWithDetailsAsync()
        {
            return await _db.Audio
                .Include(a => a.User)
                .Include(a => a.Reactions)
                    .ThenInclude(r => r.User)
                .Include(a => a.Genre)
                .ToListAsync();
        }
        public async Task<Audio> GetByIdWithDetailsAsync(Guid id)
        {
            return await _db.Audio
                .Include(a => a.User)
                .Include(a => a.Reactions)
                    .ThenInclude(r => r.User)
                .Include(a => a.Genre)
                .FirstOrDefaultAsync(a => a.Id == id);
        }
    }
}
