﻿using DL.Data;
using DL.Entities;
using DL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DL.Repositories
{
    public class GenreRepository : GenericRepository<Genre>, IGenreRepository
    {
        private readonly ApplicationContext _db;
        public GenreRepository(ApplicationContext db) : base(db)
        {
            this._db = db;
        }
        public async Task<IEnumerable<Genre>> GetAllWithDetailsAsync()
        {
            return await _db.Genres
                .Include(g => g.Audio)
                    .ThenInclude(a => a.User)
                .ToListAsync();
        }
        public async Task<Genre> GetByIdWithDetailsAsync(Guid id)
        {
            return await _db.Genres
                .Include(g => g.Audio)
                    .ThenInclude(a => a.User)
                .FirstOrDefaultAsync(g => g.Id == id);
        }
        public async Task<Genre> GetGenreByNameAsync(string genreName)
        {
            return await _db.Genres.FirstOrDefaultAsync(g => g.Name == genreName);
        }
    }
}
