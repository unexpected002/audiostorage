﻿using DL.Data;
using DL.Entities;
using DL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DL.Repositories
{
    public class GenericRepository<TEntity> : IRepository<TEntity> where TEntity : BaseEntity
    {
        private readonly ApplicationContext _db;
        public GenericRepository(ApplicationContext db)
        {
            _db = db;
        }
        public virtual async Task AddAsync(TEntity entity)
        {
            await _db.Set<TEntity>().AddAsync(entity);
        }

        public virtual async Task DeleteAsync(TEntity entity)
        {
            await Task.Run(() => _db.Set<TEntity>().Remove(entity));
        }

        public virtual async Task DeleteByIdAsync(Guid id)
        {
            TEntity item = await _db.Set<TEntity>().FindAsync(id);
            await Task.Run(() => _db.Set<TEntity>().Remove(item));
        }

        public virtual async Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await Task.Run(() => _db.Set<TEntity>());
        }

        public virtual async Task<TEntity> GetByIdAsync(Guid id)
        {
            return await _db.Set<TEntity>().FindAsync(id);
        }

        public virtual async Task UpdateAsync(TEntity entity)
        {
            await Task.Run(() =>  _db.Set<TEntity>().Update(entity));
        }
    }

}
