﻿using DL.Data;
using DL.Entities;
using DL.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly ApplicationContext _db;
        public UserRepository(ApplicationContext db)
        {
            this._db = db;
        }

        public async Task<IEnumerable<User>> GetAllWithDetailsAsync()
        {
            return await _db.Users
                .Include(u => u.Audio)
                    .ThenInclude(a => a.Genre)
                .Include(u => u.Reactions)
                .ToListAsync();
        }
        public async Task<User> GetByIdWithDetailsAsync(Guid id)
        {
            return await _db.Users
                .Include(u => u.Audio)
                    .ThenInclude(a => a.Genre)
                .Include(u => u.Reactions)
                .FirstOrDefaultAsync(u => u.Id == id);
        }
    }
}
