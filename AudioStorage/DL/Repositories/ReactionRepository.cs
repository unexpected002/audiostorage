﻿using DL.Data;
using DL.Entities;
using DL.Interfaces;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DL.Repositories
{
    public class ReactionRepository : GenericRepository<Reaction>, IReactionRepository
    {
        private readonly ApplicationContext _db;
        public ReactionRepository(ApplicationContext db) : base(db)
        {
            this._db = db;
        }

        public async Task<IEnumerable<Reaction>> GetAllWithDetailsAsync()
        {
            return await _db.Reactions
                .Include(r => r.Audio)
                .Include(r => r.User)
                .ToListAsync();
        }
        public async Task<Reaction> GetByIdWithDetailsAsync(Guid id)
        {
            return await _db.Reactions
                .Include(r => r.Audio)
                .Include(r => r.User)
                .FirstOrDefaultAsync(r => r.Id == id);
        }

        public async Task<IEnumerable<Reaction>> GetByUserIdAsync(Guid userId)
        {
            return await Task.Run(() => _db.Reactions.Where(r => r.UserId == userId));
        }
    }
}
