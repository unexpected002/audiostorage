﻿using DL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DL.Interfaces
{
    public interface IGenreRepository : IRepository<Genre>
    {
        /// <summary>
        /// Gets all genre with details asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Genre>> GetAllWithDetailsAsync();

        /// <summary>
        /// Gets the genre by identifier with details asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<Genre> GetByIdWithDetailsAsync(Guid id);

        /// <summary>
        /// Gets the genre by name asynchronous.
        /// </summary>
        /// <param name="genreName">Name of the genre.</param>
        /// <returns></returns>
        Task<Genre> GetGenreByNameAsync(string genreName);
    }
}
