﻿using DL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DL.Interfaces
{
    public interface IUserRepository
    {
        /// <summary>
        /// Gets all users with details asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<User>> GetAllWithDetailsAsync();

        /// <summary>
        /// Gets the user by identifier with details asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        Task<User> GetByIdWithDetailsAsync(Guid userId);
    }
}
