﻿using DL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DL.Interfaces
{
    public interface IAudioRepository : IRepository<Audio>
    {
        /// <summary>
        /// Gets all audio with details asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Audio>> GetAllWithDetailsAsync();

        /// <summary>
        /// Gets the audio by identifier with details asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<Audio> GetByIdWithDetailsAsync(Guid id);

    }
}
