﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DL.Interfaces
{
    public interface IUnitOfWork
    {
        /// <summary>
        /// Gets the audio repository.
        /// </summary>
        /// <value>
        /// The audio repository.
        /// </value>
        IAudioRepository AudioRepository { get; }
        /// <summary>
        /// Gets the genre repository.
        /// </summary>
        /// <value>
        /// The genre repository.
        /// </value>
        IGenreRepository GenreRepository { get; }
        /// <summary>
        /// Gets the reaction repository.
        /// </summary>
        /// <value>
        /// The reaction repository.
        /// </value>
        IReactionRepository ReactionRepository { get; }
        /// <summary>
        /// Gets the user repository.
        /// </summary>
        /// <value>
        /// The user repository.
        /// </value>
        IUserRepository UserRepository { get; }
        /// <summary>
        /// Saves changes in database the asynchronous.
        /// </summary>
        /// <returns></returns>
        Task SaveAsync();
    }
}
