﻿using DL.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DL.Interfaces
{
    public interface IReactionRepository : IRepository<Reaction>
    {
        /// <summary>
        /// Gets all reaction with details asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Reaction>> GetAllWithDetailsAsync();

        /// <summary>
        /// Gets the reaction by identifier with details asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<Reaction> GetByIdWithDetailsAsync(Guid id);

        /// <summary>
        /// Gets the reaction by user identifier asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        Task<IEnumerable<Reaction>> GetByUserIdAsync(Guid userId);
    }
}
