﻿using DL.Entities;
using DL.Enums;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace DL.Data
{
    public class ApplicationContext : IdentityDbContext<User, IdentityRole<Guid>, Guid>
    {
        public DbSet<Genre> Genres { get; set; }
        public DbSet<Reaction> Reactions { get; set; }
        public DbSet<Audio> Audio { get; set; }

        public ApplicationContext(DbContextOptions options)
        : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Genre>()
                 .ToTable("Genres")
                 .HasMany(g => g.Audio)
                 .WithOne(a => a.Genre)
                 .HasForeignKey(a => a.GenreId)
                 .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<Audio>()
                 .ToTable("Audio")
                 .HasMany(a => a.Reactions)
                 .WithOne(r => r.Audio)
                 .HasForeignKey(r => r.AudioId)
                 .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<User>()
                .HasMany(u => u.Reactions)
                .WithOne(r => r.User)
                .HasForeignKey(r => r.UserId)
                .OnDelete(DeleteBehavior.Cascade);
            modelBuilder.Entity<User>()
                .HasMany(u => u.Audio)
                .WithOne(a => a.User)
                .HasForeignKey(a => a.UserId)
                .OnDelete(DeleteBehavior.SetNull);
            modelBuilder.Entity<Reaction>()
                 .ToTable("Reactions");
            modelBuilder.Entity<User>()
                .Property(u => u.Id)
                .HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<Genre>()
                .Property(u => u.Id)
                .HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<Audio>()
                .Property(u => u.Id)
                .HasDefaultValueSql("NEWID()");
            modelBuilder.Entity<Reaction>()
                .Property(u => u.Id)
                .HasDefaultValueSql("NEWID()");
            base.OnModelCreating(modelBuilder);
        }
    }
}
