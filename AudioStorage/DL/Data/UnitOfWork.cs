﻿using DL.Interfaces;
using DL.Repositories;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace DL.Data
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly ApplicationContext _db;

        private IAudioRepository audioRepository;
        private IGenreRepository genreRepository;
        private IReactionRepository reactionRepository;
        private IUserRepository userRepository;
        public UnitOfWork(ApplicationContext db)
        {
            _db = db;
        }
        public IAudioRepository AudioRepository => audioRepository ??= new AudioRepository(_db);
        public IGenreRepository GenreRepository => genreRepository ??= new GenreRepository(_db);
        public IReactionRepository ReactionRepository => reactionRepository ??= new ReactionRepository(_db);
        public IUserRepository UserRepository => userRepository ??= new UserRepository(_db);
        public async Task SaveAsync()
        {
            await _db.SaveChangesAsync();
        }
    }
}
