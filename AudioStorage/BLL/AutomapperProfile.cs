﻿using AutoMapper;
using BLL.Models;
using BLL.Models.Audio;
using BLL.Models.User;
using DL.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BLL
{
    public class AutomapperProfile : Profile
    {
        public AutomapperProfile()
        {
            CreateMap<Audio, PresentationAudioModel>()
                .ForMember(pam => pam.Genre, a => a.MapFrom(x => x.Genre));
            CreateMap<CreationAudioModel, AudioModel>();
            CreateMap<UpdateAudioModel, AudioModel>();
            CreateMap<AudioModel, Audio>()
                .ReverseMap();
            CreateMap<Genre, GenreModel>()
                .ReverseMap();
            CreateMap<Reaction, ReactionModel>();
            CreateMap<ReactionModel, Reaction>()
                .ForMember(r => r.User, rm => rm.Ignore())
                .ForMember(r => r.Audio, rm => rm.Ignore());
            CreateMap<User, RegistrationModel>()
                .ForMember(um => um.Phone, u => u.MapFrom(x => x.PhoneNumber))
                .ForMember(um => um.Password, u => u.MapFrom(x => x.PasswordHash))
                .ReverseMap();
            CreateMap<User, SessionModel>()
                .ReverseMap();
            CreateMap<User, UserModel>()
                .ForMember(um => um.Phone, u => u.MapFrom(x => x.PhoneNumber))
                .ReverseMap();
        }
    }
}
