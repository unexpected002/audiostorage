﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BLL.Exceptions
{
    [Serializable]
    public class ModelStateException : Exception
    {
        /// <summary>Gets or sets the dictionary of validation errors.</summary>
        /// <value>The property.</value>
        public IDictionary<string, List<string>> Errors { get; set; }
        public ModelStateException(string message, IDictionary<string, List<string>> errors) : base(message)
        {
            Errors = errors;
        }
        protected ModelStateException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
