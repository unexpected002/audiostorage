﻿using System;
using System.Collections.Generic;
using System.Runtime.Serialization;
using System.Text;

namespace BLL.Exceptions
{
    [Serializable]
    public class AudioStorageException : Exception
    {
        /// <summary>Gets or sets the property of exception.</summary>
        /// <value>The property.</value>
        public string Property { get; set; }
        public AudioStorageException(string message, string prop) : base(message)
        {
            Property = prop;
        }
        protected AudioStorageException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}
