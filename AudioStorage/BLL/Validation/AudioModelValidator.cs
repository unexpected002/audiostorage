﻿using BLL.Models.Audio;
using FluentValidation;
using System;
using System.Linq;

namespace BLL.Validation
{
    public class AudioModelValidator : GenericValidator<IAudioGeneralModel>
    {
        protected const string NamePattern = @"^([A-Za-z ]+)$";
        public AudioModelValidator()
        {
            RuleFor(gm => gm.Name)
                .Must(name => isCorrectl(name, NamePattern)).WithMessage($"The audio name must have only letters!")
                .Length(MinLength, MaxLength).WithMessage($"The length of the audio name must be in range [{MinLength}, {MaxLength}]!");
            RuleFor(am => am.AuthorName)
                .Must(authorName => isCorrectl(authorName, NamePattern)).WithMessage($"The author name of the audio must have only letters!")
                .Length(MinLength, MaxLength).WithMessage($"The length of the author name of the audio must be in range [{MinLength}, {MaxLength}]!");
        }
    }
}
