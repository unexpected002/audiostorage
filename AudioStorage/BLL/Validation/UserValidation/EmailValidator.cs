﻿using BLL.Models;
using BLL.Models.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Validation.UserValidation
{
    public class EmailValidator : GenericValidator<IUserWithEmailModel>
    {
        protected const string EmailPattern = @"^(?:[\w]){3,20}@(?:[\w\-]+)(?:(?:\.(?:\w){2,3}))$";

        public EmailValidator()
        {
            RuleFor(um => um.Email)
                .Must(email => isCorrectl(email, EmailPattern)).WithMessage(@"The user email must be like this pattern ""user@gmail.com""");
        }
    }
}
