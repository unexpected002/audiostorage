﻿using BLL.Models.User;
using FluentValidation;
using System;
using System.Linq;

namespace BLL.Validation.UserValidation
{
    public class PasswordValidator : GenericValidator<IUserWithPasswordModel>
    {
        protected const int MinLengthPassword = 8;

        public PasswordValidator()
        {
            RuleFor(um => um.Password)
                .Must(p => p.Any(c => char.IsUpper(c))).WithMessage(@"The user password must contain at least one uppercase letter!")
                .Must(p => p.Any(c => char.IsDigit(c))).WithMessage(@"The user password must contain at least one digit!")
                .Length(MinLengthPassword, MaxLength).WithMessage($"The length of the user name must be in range [{MinLengthPassword}, {MaxLength}]!");
        }
    }
}
