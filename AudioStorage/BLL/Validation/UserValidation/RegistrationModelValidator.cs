﻿using BLL.Models.User;
using FluentValidation;
using System;
using System.Linq;
using System.Text.RegularExpressions;

namespace BLL.Validation.UserValidation
{
    public class RegistrationModelValidator : GenericValidator<RegistrationModel>
    {
        public RegistrationModelValidator()
        {
            Include(new UserModelValidator());
            Include(new PasswordValidator());
        }
    }
}