﻿using BLL.Models.User;
using FluentValidation;
using System;
using System.Linq;

namespace BLL.Validation.UserValidation
{
    public class AuthenticateModelValidator : GenericValidator<AuthenticateModel>
    {
        public AuthenticateModelValidator()
        {
            Include(new EmailValidator());
            Include(new PasswordValidator());
        }
    }
}