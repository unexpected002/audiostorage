﻿using BLL.Models.User;
using FluentValidation;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Validation.UserValidation
{
    public class UserModelValidator : GenericValidator<IUserWithGeneralModel>
    {
        protected const string UserNamePattern = @"^([A-Za-z]{3,}\s[A-Za-z]{3,})$";
        protected const int PhoneNumberLength = 13;
        protected const int MinAge = 10;
        protected const int MaxAge = 120;
        public UserModelValidator()
        {
            Include(new EmailValidator());
            RuleFor(um => um.UserName)
                .Must(userName => isCorrectl(userName, UserNamePattern)).WithMessage(@"The user name must be like this pattern ""Name Surname""")
                .Length(MinLength, MaxLength).WithMessage($"The length of the user name must be in range [{MinLength}, {MaxLength}]!");
            RuleFor(um => um.Phone)
                .Length(PhoneNumberLength).WithMessage($"The length of user phone must be equal to {PhoneNumberLength}");
            RuleFor(um => um.Birthdate)
                .Must(b => b.Year <= DateTime.Now.Year - MinAge && b.Year >= DateTime.Now.Year - MaxAge)
                    .WithMessage($"The year of the user birthdate must be in range [{DateTime.Now.Year - MaxAge}, {DateTime.Now.Year - MinAge}]!");
        }
    }
}