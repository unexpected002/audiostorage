﻿using BLL.Models;
using System.Linq;
using System;
using FluentValidation;

namespace BLL.Validation
{
    public class GenreModelValidator : GenericValidator<GenreModel>
    {
        protected const string NamePattern = @"^([A-Za-z ]+)$";
        public GenreModelValidator()
        {
            RuleFor(gm => gm.Name)
                .Must(name => isCorrectl(name, NamePattern)).WithMessage($"The genre name must have only letters!")
                .Length(MinLength, MaxLength).WithMessage($"The length of the genre name must be in range [{MinLength}, {MaxLength}]!");
        }
    }
}