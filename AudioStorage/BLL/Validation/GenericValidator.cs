﻿using BLL.Exceptions;
using FluentValidation;
using FluentValidation.Results;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;

namespace BLL.Validation
{
    public class GenericValidator<TEntity> : AbstractValidator<TEntity>
    {
        protected const int MinLength = 3;
        protected const int MaxLength = 20;

        public new void Validate(TEntity entity)
        {

            ValidationResult results = base.Validate(entity);
            if (!base.Validate(entity).IsValid)
            {
                var errors = new Dictionary<string, List<string>>();
                results.Errors.ForEach(e =>
                {
                    if(!errors.ContainsKey(e.PropertyName))
                    {
                        errors.Add(e.PropertyName, new List<string>());
                    }
                    errors[e.PropertyName].Add(e.ErrorMessage);
                });
                throw new ModelStateException($"{entity.GetType().Name} model is incorrect!", errors);
            }
        }

        protected bool isCorrectl(string field, string pattern)
        {
            Regex regex = new Regex(pattern);
            Match match = regex.Match(field);
            return match.Success;
        }
    }
}