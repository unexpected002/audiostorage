﻿using BLL.Models.Audio;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IAudioService
    {
        /// <summary>
        /// Gets all audio asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<PresentationAudioModel>> GetAllAsync();

        /// <summary>
        /// Gets the audio by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<PresentationAudioModel> GetByIdAsync(Guid id);

        /// <summary>
        /// Adds the audio asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task AddAsync(AudioModel model);

        /// <summary>
        /// Updates the audio asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task UpdateAsync(AudioModel model);

        /// <summary>
        /// Deletes the audio asynchronous.
        /// </summary>
        /// <param name="modelId">The model identifier.</param>
        /// <returns></returns>
        Task DeleteAsync(Guid modelId);

        /// <summary>
        /// Downloads the audio asynchronous.
        /// </summary>
        /// <param name="audioId">The audio identifier.</param>
        /// <returns></returns>
        public Task<AudioFileModel> DownloadAsync(Guid audioId);
    }
}
