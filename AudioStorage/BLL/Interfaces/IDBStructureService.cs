﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IDBStructureService
    {
        /// <summary>
        /// Initializes the database.
        /// </summary>
        /// <returns></returns>
        public Task InitializeDB();
    }
}
