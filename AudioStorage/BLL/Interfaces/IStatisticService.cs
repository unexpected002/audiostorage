﻿using BLL.Models.Audio;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IStatisticService
    {
        /// <summary>
        /// Gets the most popular audio by genre asynchronous.
        /// </summary>
        /// <param name="genreId">The genre identifier.</param>
        /// <returns></returns>
        Task<IEnumerable<PresentationAudioModel>> GetGenresMostPopularAudioAsync(Guid genreId);
        /// <summary>
        /// Gets the liked audio by user identifier asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        Task<IEnumerable<PresentationAudioModel>> GetLikedAudioAsync(Guid userId);
        /// <summary>
        /// Gets the posted audio by user identifier asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        Task<IEnumerable<PresentationAudioModel>> GetPostedAudioAsync(Guid userId);
    }
}
