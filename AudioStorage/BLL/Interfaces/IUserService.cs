﻿using BLL.Models;
using BLL.Models.User;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IUserService
    {
        /// <summary>
        /// Gets all users asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<UserModel>> GetAllAsync();

        /// <summary>
        /// Gets the user by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<UserModel> GetByIdAsync(Guid id);

        /// <summary>
        /// Adds the user asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task AddAsync(RegistrationModel model);

        /// <summary>
        /// Updates the user asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task UpdateAsync(UserModel model);

        /// <summary>
        /// Deletes the user asynchronous.
        /// </summary>
        /// <param name="modelId">The model identifier.</param>
        /// <returns></returns>
        Task DeleteAsync(Guid modelId);

        /// <summary>
        /// Authenticates the user specified model.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task<SessionModel> Authenticate(AuthenticateModel model);

        /// <summary>
        /// Gets the reaction by user identifier asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        Task<IEnumerable<ReactionModel>> GetReactionByUserIdAsync(Guid userId);

        /// <summary>
        /// Changes the user password.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <param name="newPasswordModel">The new password model.</param>
        /// <returns></returns>
        Task ChangePassword(Guid userId, NewPasswordModel newPasswordModel);
    }
}
