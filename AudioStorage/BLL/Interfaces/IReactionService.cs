﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IReactionService
    {
        /// <summary>
        /// Gets all reactions asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<ReactionModel>> GetAllAsync();

        /// <summary>
        /// Adds the reaction asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task AddAsync(ReactionModel model);

        /// <summary>
        /// Gets the reaction by user identifier asynchronous.
        /// </summary>
        /// <param name="userId">The user identifier.</param>
        /// <returns></returns>
        Task<IEnumerable<ReactionModel>> GetByUserIdAsync(Guid userId);
    }
}
