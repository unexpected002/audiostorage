﻿using BLL.Models;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Interfaces
{
    public interface IGenreService
    {
        /// <summary>
        /// Gets all genres asynchronous.
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<GenreModel>> GetAllAsync();

        /// <summary>
        /// Gets the genre by identifier asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task<GenreModel> GetByIdAsync(Guid id);

        /// <summary>
        /// Adds the genre asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task AddAsync(GenreModel model);

        /// <summary>
        /// Updates the genre asynchronous.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        Task UpdateAsync(GenreModel model);

        /// <summary>
        /// Deletes the genre asynchronous.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        Task DeleteAsync(Guid id);
    }
}
