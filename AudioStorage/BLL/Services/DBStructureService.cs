﻿using BLL.Interfaces;
using DL.Data;
using DL.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using Microsoft.Extensions.Hosting;
using System.Threading.Tasks;
using DL.Enums;

namespace BLL.Services
{
    public class DBStructureService : IDBStructureService
    {
        private readonly ApplicationContext _db;
        private readonly UserManager<User> _userManager;
        private readonly RoleManager<IdentityRole<Guid>> _roleManager;
        private readonly IHostEnvironment _env;
        private readonly string DefaultPassword = "P@$$w0rd";
        public DBStructureService(ApplicationContext db, UserManager<User> userManager, RoleManager<IdentityRole<Guid>> roleManager, IHostEnvironment env)
        {
            _db = db;
            _userManager = userManager;
            _roleManager = roleManager;
            _env = env;
        }

        public async Task InitializeDB()
        {
            await _db.Database.MigrateAsync();

            await SeedRequireData();

            if (_env.IsDevelopment())
            {
                await SeedTestData();
            }

            await _db.SaveChangesAsync();
        }

        private async Task SeedRequireData()
        {
            var roles = new List<IdentityRole<Guid>>
            {
                new IdentityRole<Guid>
                {
                    Id = new Guid("759d096b-ea61-426d-a617-23593ba843a3"),
                    Name = "Administrator"
                },
                new IdentityRole<Guid>
                {
                    Id = new Guid("ad8e7930-efb6-4bc4-9b73-3c23bfad80d5"),
                    Name = "User"
                }
            };
            foreach (var role in roles)
            {
                if (await _roleManager.FindByIdAsync(role.Id.ToString()) == null)
                {
                    await _roleManager.CreateAsync(role);
                }
            }
            var admin = new User
            {
                Id = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                UserName = "Leonov Petro",
                Email = "admin@gmail.com",
                PhoneNumber = "+380964567723",
                Birthdate = new DateTime(2001, 3, 20)
            };
            if (await _userManager.FindByIdAsync(admin.Id.ToString()) == null)
            {
                await _userManager.CreateAsync(admin);
                await _userManager.AddPasswordAsync(admin, DefaultPassword);
                await _userManager.AddToRoleAsync(admin, "Administrator");
            }
        }

        private async Task SeedTestData()
        {
            var users = new List<User>
            {
                new User
                {
                    Id = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    UserName = "Valte Morte",
                    Email = "user1@gmail.com",
                    PhoneNumber = "+380957689453",
                    Birthdate = new DateTime(1997, 1, 6)
                },
                new User
                {
                    Id = new Guid("e8bb6d2e-2be0-48c4-baeb-4ee96de9f328"),
                    UserName = "Kaan Currie",
                    Email = "user2@gmail.com",
                    PhoneNumber = "+380504230462",
                    Birthdate = new DateTime(1999, 12, 1)
                },
                new User
                {
                    Id = new Guid("f3626d23-ee46-4c5c-9a7c-63b3da7e64cc"),
                    UserName = "Nathalie Kavanagh",
                    Email = "user3@gmail.com",
                    PhoneNumber = "+380522558700",
                    Birthdate = new DateTime(1994, 9, 16)
                },
                new User
                {
                    Id = new Guid("e93488b8-83fa-4bf2-9e6f-39fc38e84996"),
                    UserName = "Janice Mays",
                    Email = "user4@gmail.com",
                    PhoneNumber = "+380692464059",
                    Birthdate = new DateTime(1990, 7, 26)
                }
            };
            var genres = new List<Genre>
            {
                new Genre { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Rock" },
                new Genre { Id = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"), Name = "Hip-hop" },
                new Genre { Id = new Guid("b1d28aa9-98c2-495a-a44c-fdb70af4b6da"), Name = "Juzz" }
            };
            var audios = new List<Audio>
            {
                #region Rock
                    new Audio
                    {
                        Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                        GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                        Name = "Make Funk",
                        AuthorName = "HoliznaCC0",
                        PostedDate = new DateTime(2022, 6, 18),
                        UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                    },
                    new Audio
                    {
                        Id = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                        GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                        Name = "GROOOVY TRAP",
                        AuthorName = "Caslo",
                        PostedDate = new DateTime(2022, 6, 13),
                        UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                    },
                    new Audio
                    {
                        Id = new Guid("911c0182-b561-4eec-a03b-0291e845bf97"),
                        GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                        Name = "Stop The Kill",
                        AuthorName = "John Lopker",
                        PostedDate = new DateTime(2021, 9, 17),
                        UserId = new Guid("e8bb6d2e-2be0-48c4-baeb-4ee96de9f328")
                    },
                    new Audio
                    {
                        Id = new Guid("0a64818a-75ab-4035-abdf-ebb8844f4323"),
                        GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                        Name = "Inside Out",
                        AuthorName = "Taylor Poe",
                        PostedDate = new DateTime(2022, 11, 19),
                        UserId = new Guid("e8bb6d2e-2be0-48c4-baeb-4ee96de9f328")
                    },
                    new Audio
                    {
                        Id = new Guid("01fa709e-c732-4021-a32e-4aea6f7b3b78"),
                        GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                        Name = "Strijp",
                        AuthorName = "Maarten Schellekens",
                        PostedDate = new DateTime(2021, 12, 1),
                        UserId = new Guid("e8bb6d2e-2be0-48c4-baeb-4ee96de9f328")
                    },
                #endregion
                #region Hip-hop
                    new Audio
                    {
                        Id = new Guid("6f4465ef-7da4-4743-9b2a-435728d37fcb"),
                        GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                        Name = "magna crusher",
                        AuthorName = "kaleidoplasm",
                        PostedDate = new DateTime(2021, 6, 23),
                        UserId = new Guid("e93488b8-83fa-4bf2-9e6f-39fc38e84996")
                    },
                    new Audio
                    {
                        Id = new Guid("b4fd478e-a0be-4ff3-b266-8cdda7a7f037"),
                        GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                        Name = "Pressure",
                        AuthorName = "Makaih Beats",
                        PostedDate = new DateTime(2020, 2, 10),
                        UserId = new Guid("e93488b8-83fa-4bf2-9e6f-39fc38e84996")
                    },
                    new Audio
                    {
                        Id = new Guid("3239680e-4ef0-4c37-b0a9-2a3527930bd7"),
                        GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                        Name = "Dancing Ghosts",
                        AuthorName = "HoliznaPATREON",
                        PostedDate = new DateTime(2020, 3, 23),
                        UserId = new Guid("e8bb6d2e-2be0-48c4-baeb-4ee96de9f328")
                    },
                    new Audio
                    {
                        Id = new Guid("b6259714-d486-4194-98a8-bbe6b64037a5"),
                        GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                        Name = "Presets",
                        AuthorName = "Ketsa",
                        PostedDate = new DateTime(2022, 1, 1),
                        UserId = new Guid("f3626d23-ee46-4c5c-9a7c-63b3da7e64cc")
                    },
                    new Audio
                    {
                        Id = new Guid("5104c096-07dc-4449-aa52-1d546c881e1e"),
                        GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                        Name = "Visualize",
                        AuthorName = "Makaih Beats",
                        PostedDate = new DateTime(2022, 3, 26),
                        UserId = new Guid("e93488b8-83fa-4bf2-9e6f-39fc38e84996")
                    },
                #endregion
                #region Juzz
                    new Audio
                    {
                        Id = new Guid("73c2780a-b2c4-49f1-96cc-54af3be64f15"),
                        GenreId = new Guid("b1d28aa9-98c2-495a-a44c-fdb70af4b6da"),
                        Name = "Dancing Ghosts",
                        AuthorName = "HoliznaPATREON",
                        PostedDate = new DateTime(2020, 12, 2),
                        UserId = new Guid("f3626d23-ee46-4c5c-9a7c-63b3da7e64cc")
                    },
                    new Audio
                    {
                        Id = new Guid("41d99a3d-9f03-4eeb-b5b4-cd525cf75e85"),
                        GenreId = new Guid("b1d28aa9-98c2-495a-a44c-fdb70af4b6da"),
                        Name = "A little better",
                        AuthorName = "Alex Figueira",
                        PostedDate = new DateTime(2022, 9, 15),
                        UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2")
                    },
                    new Audio
                    {
                        Id = new Guid("3a92f789-c755-499e-bfe8-a122631f23cf"),
                        GenreId = new Guid("b1d28aa9-98c2-495a-a44c-fdb70af4b6da"),
                        Name = "Jazzy Lounge",
                        AuthorName = "Maarten Schellekens",
                        PostedDate = new DateTime(2020, 7, 4),
                        UserId = new Guid("f3626d23-ee46-4c5c-9a7c-63b3da7e64cc")
                    },
                    new Audio
                    {
                        Id = new Guid("b5ec7587-936f-4818-a247-8f35d0e8e0d5"),
                        GenreId = new Guid("b1d28aa9-98c2-495a-a44c-fdb70af4b6da"),
                        Name = "Longe",
                        AuthorName = "Alex Figueira",
                        PostedDate = new DateTime(2020, 8, 21),
                        UserId = new Guid("f3626d23-ee46-4c5c-9a7c-63b3da7e64cc")
                    },
                    new Audio
                    {
                        Id = new Guid("ec39a496-dc10-4028-a95c-2607eacd2647"),
                        GenreId = new Guid("b1d28aa9-98c2-495a-a44c-fdb70af4b6da"),
                        Name = "Resulting Matters",
                        AuthorName = "Ketsa",
                        PostedDate = new DateTime(2021, 4, 27),
                        UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                    }
                    #endregion
            };
            var reactions = new List<Reaction>
            {
                new Reaction
                {
                    Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    AudioId = new Guid("6f4465ef-7da4-4743-9b2a-435728d37fcb"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("7e83c182-532d-4440-a3b6-9a9ad8cebf0f"),
                    UserId = new Guid("e8bb6d2e-2be0-48c4-baeb-4ee96de9f328"),
                    AudioId = new Guid("3239680e-4ef0-4c37-b0a9-2a3527930bd7"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("92cdb8cb-a0be-4a49-9f47-94cb09de20f3"),
                    UserId = new Guid("e8bb6d2e-2be0-48c4-baeb-4ee96de9f328"),
                    AudioId = new Guid("3a92f789-c755-499e-bfe8-a122631f23cf"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("a3cb196e-0b3b-47fc-b92f-68e5ee4a2b5d"),
                    UserId = new Guid("e93488b8-83fa-4bf2-9e6f-39fc38e84996"),
                    AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("a2cba7a0-83b0-448d-9da3-dbf28e8e6a45"),
                    UserId = new Guid("e8bb6d2e-2be0-48c4-baeb-4ee96de9f328"),
                    AudioId = new Guid("ec39a496-dc10-4028-a95c-2607eacd2647"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("a2cba7a0-83b0-448d-9da3-dbf28e8e6a45"),
                    UserId = new Guid("f3626d23-ee46-4c5c-9a7c-63b3da7e64cc"),
                    AudioId = new Guid("0a64818a-75ab-4035-abdf-ebb8844f4323"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("a2cba7a0-83b0-448d-9da3-dbf28e8e6a45"),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    AudioId = new Guid("5104c096-07dc-4449-aa52-1d546c881e1e"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("a2cba7a0-83b0-448d-9da3-dbf28e8e6a45"),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    AudioId = new Guid("b6259714-d486-4194-98a8-bbe6b64037a5"),
                    Rate = Rate.Like
                }
            };
            foreach (var user in users)
            {
                if (await _userManager.FindByIdAsync(user.Id.ToString()) == null)
                {
                    await _userManager.CreateAsync(user);
                    await _userManager.AddPasswordAsync(user, DefaultPassword);
                    await _userManager.AddToRoleAsync(user, "User");
                }
            }
            foreach (var genre in genres)
            {
                if (await _db.Genres.FindAsync(genre.Id) == null)
                {
                    _db.Genres.Add(genre);
                }
            }
            foreach (var audio in audios)
            {
                if (await _db.Audio.FindAsync(audio.Id) == null)
                {
                    _db.Audio.Add(audio);
                }
            }
            foreach (var reaction in reactions)
            {
                if (await _db.Reactions.FindAsync(reaction.Id) == null)
                {
                    _db.Reactions.Add(reaction);
                }
            };
        }
    }
}
