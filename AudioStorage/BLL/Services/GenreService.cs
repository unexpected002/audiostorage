﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models;
using BLL.Validation;
using DL.Entities;
using DL.Interfaces;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class GenreService : IGenreService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public GenreService(IUnitOfWork db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task AddAsync(GenreModel genreModel)
        {
            if (await _db.GenreRepository.GetGenreByNameAsync(genreModel.Name) != null)
            {
                throw new AudioStorageException("This genre name already exists!", nameof(genreModel.Name));
            }
            await _db.GenreRepository.AddAsync(_mapper.Map<Genre>(genreModel));
            await _db.SaveAsync();
        }

        public async Task DeleteAsync(Guid genreId)
        {
            await _db.GenreRepository.DeleteByIdAsync(genreId);
            await _db.SaveAsync();
        }

        public async Task<IEnumerable<GenreModel>> GetAllAsync()
        {
            var genres = await _db.GenreRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<GenreModel>>(genres);
        }

        public async Task<GenreModel> GetByIdAsync(Guid genreId)
        {
            var genre = await _db.GenreRepository.GetByIdWithDetailsAsync(genreId);
            return _mapper.Map<GenreModel>(genre);
        }

        public async Task UpdateAsync(GenreModel genreModel)
        {
            if (await _db.GenreRepository.GetGenreByNameAsync(genreModel.Name) != null)
            {
                throw new AudioStorageException("This genre name already exists!", nameof(genreModel.Name));
            }
            await Task.Run(() => _db.GenreRepository.UpdateAsync(_mapper.Map<Genre>(genreModel)));
            await _db.SaveAsync();
        }
    }
}
