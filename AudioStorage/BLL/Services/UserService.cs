﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Helpers;
using BLL.Interfaces;
using BLL.Models;
using BLL.Models.User;
using BLL.Validation;
using DL.Entities;
using DL.Interfaces;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class UserService : IUserService
    {
        private readonly IUnitOfWork _db;
        private readonly UserManager<User> _userManager;
        private readonly IMapper _mapper;
        private readonly AppSettings _appSettings;

        private const string DefaultRole = "User";

        public UserService(IUnitOfWork db, IMapper mapper, IOptions<AppSettings> appSettings, UserManager<User> userManager)
        {
            _db = db;
            _mapper = mapper;
            _appSettings = appSettings.Value;
            _userManager = userManager;
        }

        public async Task AddAsync(RegistrationModel userModel)
        {
            if(await Task.Run(() => _userManager.Users.Any(u => u.PhoneNumber == userModel.Phone)))
            {
                throw new AudioStorageException("This phone number already exists!", nameof(userModel.Phone));
            }
            var user = _mapper.Map<User>(userModel);
            var result = await _userManager.CreateAsync(user, userModel.Password);
            if (!result.Succeeded)
            {
                throw new AudioStorageException("This email address already exists!", nameof(userModel.Email));
            }
            user = await _userManager.FindByEmailAsync(user.Email);
            await _userManager.AddToRoleAsync(user, DefaultRole);
            await _db.SaveAsync();
        }

        public async Task DeleteAsync(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            await _userManager.DeleteAsync(user);
            await _db.SaveAsync();
        }

        public async Task<IEnumerable<UserModel>> GetAllAsync()
        {
            var users = await _userManager.Users.ToListAsync();
            return _mapper.Map<IEnumerable<UserModel>>(users);
        }

        public async Task<UserModel> GetByIdAsync(Guid userId)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());
            return _mapper.Map<UserModel>(user);
        }

        public async Task UpdateAsync(UserModel userModel)
        {
            var user = await _userManager.FindByIdAsync(userModel.Id.ToString());
            _mapper.Map(userModel, user);
            await _userManager.UpdateAsync(user);
            await _db.SaveAsync();
        }

        public async Task<IEnumerable<ReactionModel>> GetReactionByUserIdAsync(Guid userId)
        {
            var reactions = (await _db.UserRepository.GetByIdWithDetailsAsync(userId)).Reactions;
            return _mapper.Map< IEnumerable<ReactionModel>>(reactions);
        }

        public async Task<SessionModel> Authenticate(AuthenticateModel model)
        {
            var user = await _userManager.FindByEmailAsync(model.Email);

            var isCorrect = await _userManager.CheckPasswordAsync(user, model.Password);

            if (!isCorrect)
            {
                return null;
            }

            var roles = await _userManager.GetRolesAsync(user);

            if(!roles.Any())
            {
                return null;
            }

            var sessionModel = _mapper.Map<SessionModel>(user);
            sessionModel.Role = roles;

            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes(_appSettings.Secret);
            var claimsIdentity = new ClaimsIdentity();
            claimsIdentity.AddClaim(new Claim(ClaimTypes.Name, sessionModel.Id.ToString()));
            foreach (var role in roles)
            {
                claimsIdentity.AddClaim(new Claim(ClaimTypes.Role, role));
            }
            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = claimsIdentity,
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            var token = tokenHandler.CreateToken(tokenDescriptor);
            sessionModel.Token = tokenHandler.WriteToken(token);

            return sessionModel;
        }

        public async Task ChangePassword(Guid userId, NewPasswordModel newPasswordModel)
        {
            var user = await _userManager.FindByIdAsync(userId.ToString());

            var isCorrect = await _userManager.ChangePasswordAsync(user, newPasswordModel.OldPassword, newPasswordModel.Password);

            if (!isCorrect.Succeeded)
            {
                throw new AudioStorageException("Your password is incorrect!", newPasswordModel.OldPassword);
            }
        }
    }
}
