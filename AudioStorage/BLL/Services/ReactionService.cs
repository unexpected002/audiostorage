﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using DL.Entities;
using DL.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class ReactionService : IReactionService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;

        public ReactionService(IUnitOfWork db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task AddAsync(ReactionModel reactionModel)
        {
            await _db.ReactionRepository.AddAsync(_mapper.Map<Reaction>(reactionModel));
            await _db.SaveAsync();
        }

        public async Task<IEnumerable<ReactionModel>> GetAllAsync()
        {
            var reactions = await _db.ReactionRepository.GetAllAsync();
            return _mapper.Map<IEnumerable<ReactionModel>>(reactions);
        }

        public async Task<IEnumerable<ReactionModel>> GetByUserIdAsync(Guid userId)
        {
            var reactions = await _db.ReactionRepository.GetByUserIdAsync(userId);
            return _mapper.Map<IEnumerable<ReactionModel>>(reactions);
        }
    }
}
