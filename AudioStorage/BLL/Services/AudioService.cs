﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models.Audio;
using BLL.Validation;
using DL.Entities;
using DL.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class AudioService : IAudioService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;
        private const string DefaultPath = "Audio";
        private const string DefaultExtention = ".mp3";

        public AudioService(IUnitOfWork db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task AddAsync(AudioModel audioModel)
        {
            await _db.AudioRepository.AddAsync(_mapper.Map<Audio>(audioModel));
            await _db.SaveAsync();
        }

        public async Task DeleteAsync(Guid audioId)
        {
            await _db.AudioRepository.DeleteByIdAsync(audioId);
            await _db.SaveAsync();
        }

        public async Task<IEnumerable<PresentationAudioModel>> GetAllAsync()
        {
            var audio = await _db.AudioRepository.GetAllWithDetailsAsync();
            return _mapper.Map<IEnumerable<PresentationAudioModel>>(audio);
        }

        public async Task<PresentationAudioModel> GetByIdAsync(Guid audioId)
        {
            var audio = await _db.AudioRepository.GetByIdWithDetailsAsync(audioId);
            return _mapper.Map<PresentationAudioModel>(audio);
        }

        public async Task UpdateAsync(AudioModel audioModel)
        {
            await _db.AudioRepository.UpdateAsync(_mapper.Map<Audio>(audioModel));
            await _db.SaveAsync();
        }

        public async Task<AudioFileModel> DownloadAsync(Guid audioId)
        {
            var audio = await _db.AudioRepository.GetByIdAsync(audioId);
            if (audio != null)
            {
                var audioFileModel = new AudioFileModel();

                var audioName = audio.Id.ToString().ToUpper() + DefaultExtention;
                var currentDirectory = Path.Combine(Directory.GetCurrentDirectory(), DefaultPath);
                var audioFile = Path.Combine(currentDirectory, audioName);

                MemoryStream ms = new MemoryStream();
                using (var stream = new FileStream(audioFile, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    await stream.CopyToAsync(ms);
                }

                audioFileModel.AudioName = audio.AuthorName + " - " + audio.Name + DefaultExtention;
                audioFileModel.AudioData = ms.ToArray();

                return audioFileModel;
            }
            return null;
        }
    }
}
