﻿using AutoMapper;
using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models.Audio;
using DL.Enums;
using DL.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BLL.Services
{
    public class StatisticService : IStatisticService
    {
        private readonly IUnitOfWork _db;
        private readonly IMapper _mapper;
        private const int AmountAudio = 3;

        public StatisticService(IUnitOfWork db, IMapper mapper)
        {
            _db = db;
            _mapper = mapper;
        }

        public async Task<IEnumerable<PresentationAudioModel>> GetLikedAudioAsync(Guid userId)
        {
            var audio = (await _db.UserRepository.GetAllWithDetailsAsync())
                .Where(u => u.Id == userId)
                .SelectMany(u => u.Reactions)
                .Where(r => r.Rate == Rate.Like)
                .Select(r => r.Audio);
            if(!audio.Any())
            {
                return null;
            }
            return _mapper.Map<IEnumerable<PresentationAudioModel>>(audio);
        }

        public async Task<IEnumerable<PresentationAudioModel>> GetGenresMostPopularAudioAsync(Guid genreId)
        {
            var audio = (await _db.ReactionRepository.GetAllWithDetailsAsync())
                .GroupBy(r => r.Audio, (key, r) => new
                {
                    Audio = key,
                    Likes = r.Where(r => r.Rate == Rate.Like).Count()
                })
                .OrderByDescending(gr => gr.Likes)
                .Select(gr => gr.Audio)
                .Where(a => a.GenreId == genreId)
                .Take(AmountAudio);
            if (!audio.Any())
            {
                return null;
            }
            return _mapper.Map<IEnumerable<PresentationAudioModel>>(audio);
        }

        public async Task<IEnumerable<PresentationAudioModel>> GetPostedAudioAsync(Guid userId)
        {
            var audio = (await _db.UserRepository.GetAllWithDetailsAsync())
                .Where(u => u.Id == userId)
                .SelectMany(u => u.Audio);
            if (!audio.Any())
            {
                return null;
            }
            return _mapper.Map<IEnumerable<PresentationAudioModel>>(audio);
        }
    }
}
