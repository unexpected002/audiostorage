﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.User
{
    public class UserModel : IUserWithGeneralModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime Birthdate { get; set; }
    }
}
