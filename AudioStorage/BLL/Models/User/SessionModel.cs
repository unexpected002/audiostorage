﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.User
{
    public class SessionModel
    {
        public Guid Id { get; set; }
        public string UserName { get; set; }
        public IEnumerable<string> Role { get; set; }
        public string Token { get; set; }
    }
}
