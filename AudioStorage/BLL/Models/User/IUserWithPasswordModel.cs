﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.User
{
    public interface IUserWithPasswordModel
    {
        public string Password { get; set; }
    }
}
