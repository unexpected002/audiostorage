﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.User
{
    public interface IUserWithEmailModel
    {
        public string Email { get; set; }
    }
}
