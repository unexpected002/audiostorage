﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.User
{
    public class RegistrationModel : IUserWithGeneralModel, IUserWithPasswordModel
    {
        public string UserName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public DateTime Birthdate { get; set; }
        public string Password { get; set; }
    }
}