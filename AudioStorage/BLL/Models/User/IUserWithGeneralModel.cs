﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.User
{
    public interface IUserWithGeneralModel : IUserWithEmailModel
    {
        public string UserName { get; set; }
        public string Phone { get; set; }
        public DateTime Birthdate { get; set; }
    }
}
