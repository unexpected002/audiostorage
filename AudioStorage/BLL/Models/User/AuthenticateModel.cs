﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace BLL.Models.User
{
    public class AuthenticateModel : IUserWithEmailModel, IUserWithPasswordModel
    {
        public string Email { get; set; }
        public string Password { get; set; }
    }
}
