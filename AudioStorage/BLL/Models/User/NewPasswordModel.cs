﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.User
{
    public class NewPasswordModel : IUserWithPasswordModel
    {
        public string OldPassword { get; set; }
        public string Password { get; set; }
    }
}
