﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.Audio
{
    public interface IAudioGeneralModel
    {
        public string Name { get; set; }
        public string AuthorName { get; set; }
    }
}
