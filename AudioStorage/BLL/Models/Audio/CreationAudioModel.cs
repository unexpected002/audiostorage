﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.Audio
{
    public class CreationAudioModel : IAudioGeneralModel
    {
        public Guid GenreId { get; set; }
        public string Name { get; set; }
        public string AuthorName { get; set; }
    }
}