﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.Audio
{
    public class PresentationAudioModel : IAudioGeneralModel
    {
        public Guid Id { get; set; }
        public GenreModel Genre { get; set; }
        public string Name { get; set; }
        public string AuthorName { get; set; }
        public DateTime PostedDate { get; set; }
    }
}