﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.Audio
{
    public class AudioFileModel
    {
        public string AudioName { get; set; }
        public byte[] AudioData { get; set; }
    }
}
