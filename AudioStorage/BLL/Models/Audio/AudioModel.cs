﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models.Audio
{
    public class AudioModel : IAudioGeneralModel
    {
        public Guid Id { get; set; }
        public Guid GenreId { get; set; }
        public string Name { get; set; }
        public string AuthorName { get; set; }
        public DateTime PostedDate { get; set; }
        public Guid? UserId { get; set; }
    }
}
