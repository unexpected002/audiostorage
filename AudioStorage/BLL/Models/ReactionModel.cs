﻿using DL.Enums;
using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class ReactionModel
    {
        public Guid Id { get; set; }
        public Guid UserId { get; set; }
        public Guid AudioId { get; set; }
        public Rate Rate { get; set; }
    }
}
