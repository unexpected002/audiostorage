﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Models
{
    public class GenreModel
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
    }
}
