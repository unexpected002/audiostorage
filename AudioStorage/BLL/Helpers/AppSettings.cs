﻿using System;
using System.Collections.Generic;
using System.Text;

namespace BLL.Helpers
{
    public class AppSettings
    {
        /// <summary>Gets or sets the secret for JWT.</summary>
        /// <value>The secret.</value>
        public string Secret { get; set; }
    }
}
