﻿using BLL.Models;
using BLL.Models.Audio;
using BLL.Services;
using DL.Entities;
using DL.Enums;
using DL.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioStorage.Tests.BusinessTests
{
    public class StatisticServiceTests
    {
        [TestCase("d474c995-32fe-4176-ad1c-a0f77b466396")]
        public async Task AudioService_GetLikedAudioByUserId_ReturnsAudio(Guid userId)
        {
            //arrange
            var expected = GetTestLikedAudioModels;
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(uow => uow.UserRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(GetTestUserEntities.AsEnumerable());
            var statisticService = new StatisticService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await statisticService.GetLikedAudioAsync(userId);

            //assert
            actual.Should().BeEquivalentTo(expected);
        }
        [TestCase("d474c995-32fe-4176-ad1c-a0f77b466396")]
        public async Task AudioService_GetPostedAudioByUserId_ReturnsAudio(Guid userId)
        {
            //arrange
            var expected = GetTestPostedAudioModels;
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(uow => uow.UserRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(GetTestUserEntities.AsEnumerable());
            var statisticService = new StatisticService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await statisticService.GetPostedAudioAsync(userId);

            //assert
            actual.Should().BeEquivalentTo(expected);
        }
        #region TestData
        public List<PresentationAudioModel> GetTestLikedAudioModels =>
            new List<PresentationAudioModel>()
            {
                new PresentationAudioModel
                {
                    Id = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    Genre = GetTestGenreModel[1],
                    Name = "Lazer Tag",
                    AuthorName = "SUPERARE",
                    PostedDate = new DateTime(2022, 6, 13)
                }
            };
        public List<PresentationAudioModel> GetTestPostedAudioModels =>
            new List<PresentationAudioModel>()
            {
                new PresentationAudioModel
                {
                    Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                    Genre = GetTestGenreModel[0],
                    Name = "Make Funk",
                    AuthorName = "HoliznaCC0",
                    PostedDate = new DateTime(2022, 6, 18)
                },
                new PresentationAudioModel
                {
                    Id = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    Genre = GetTestGenreModel[0],
                    Name = "GROOOVY TRAP",
                    AuthorName = "Caslo",
                    PostedDate = new DateTime(2022, 6, 13)
                }
            };
        public List<GenreModel> GetTestGenreModel =>
           new List<GenreModel>()
           {
                new GenreModel { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Rock" },
                new GenreModel { Id = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"), Name = "Hip-hop" }
           };
        public List<Genre> GetTestGenreEntities =>
           new List<Genre>()
           {
                new Genre { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Rock" },
                new Genre { Id = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"), Name = "Hip-hop" }
           };
        public List<Audio> GetTestAudioEntities =>
            new List<Audio>()
            {
                new Audio
                {
                    Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "Make Funk",
                    AuthorName = "HoliznaCC0",
                    PostedDate = new DateTime(2022, 6, 18),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    Genre = GetTestGenreEntities[0]
                },
                new Audio
                {
                    Id = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "GROOOVY TRAP",
                    AuthorName = "Caslo",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    Genre = GetTestGenreEntities[0]
                },
                new Audio
                {
                    Id = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                    Name = "Lazer Tag",
                    AuthorName = "SUPERARE",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                    Genre = GetTestGenreEntities[1]
                }
            };
        public List<User> GetTestUserEntities =>
            new List<User>()
            {
                new User
                {
                    Id = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                    UserName = "Leonov Petro",
                    Email = "admin@gmail.com",
                    Birthdate = new DateTime(2001, 3, 20),
                    ConcurrencyStamp = "bcd4229e-ca9c-4ddc-a035-a5a7a43a4d37",
                    Reactions = new List<Reaction>()
                    {
                        new Reaction
                        {
                            Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                            UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                            AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                            Rate = Rate.Like,
                            Audio = GetTestAudioEntities[1]
                        }
                    },
                    Audio =  new List<Audio>
                    {
                        GetTestAudioEntities[2]
                    }
                },
                new User
                {
                    Id = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    Email = "user@gmail.com",
                    UserName = "Valte Morte",
                    Birthdate = new DateTime(1997, 1, 6),
                    ConcurrencyStamp = "c3a0d7d0-cb44-4866-b6ec-18e4caa48b80",
                    Reactions = new List<Reaction>()
                    {
                        new Reaction
                        {
                            Id = new Guid("4ba7231c-04f3-4a02-988d-8c932068c6d2"),
                            UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                            AudioId = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                            Rate = Rate.Like,
                            Audio = GetTestAudioEntities[2]
                        }
                    },
                    Audio =  new List<Audio>
                    {
                        GetTestAudioEntities[0],
                        GetTestAudioEntities[1]
                    }
                }
            };
        #endregion
    }
}
