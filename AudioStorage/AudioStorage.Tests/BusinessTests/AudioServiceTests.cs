﻿using BLL.Models;
using BLL.Models.Audio;
using BLL.Services;
using DL.Entities;
using DL.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudioStorage.Tests.BusinessTests
{
    public class AudioServiceTests
    {
        [TestCase("758bd178-dff9-42c5-8924-50bdfafdfa47")]
        [TestCase("91a2d7f3-bbf8-4001-a571-54f739b0c791")]
        [TestCase("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea")]
        public async Task AudioService_GetById_ReturnsAudioModel(Guid audioId)
        {
            //arrange
            var expected = GetTestAudioModels.First();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(uow => uow.AudioRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>()))
                .ReturnsAsync(GetTestAudioEntities.First());
            var audioService = new AudioService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await audioService.GetByIdAsync(audioId);

            //assert
            actual.Should().BeEquivalentTo(expected);
        }
        [Test]
        public async Task AudioService_GetAll_ReturnsAllAudio()
        {
            //arrange
            var expected = GetTestAudioModels;
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(uow => uow.AudioRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(GetTestAudioEntities.AsEnumerable());
            var audioService = new AudioService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await audioService.GetAllAsync();

            //assert
            actual.Should().BeEquivalentTo(expected);
        }
        #region TestData
        public List<PresentationAudioModel> GetTestAudioModels =>
            new List<PresentationAudioModel>()
            {
                new PresentationAudioModel
                {
                    Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                    Genre = GetTestGenreModel[0],
                    Name = "Make Funk",
                    AuthorName = "HoliznaCC0",
                    PostedDate = new DateTime(2022, 6, 18)
                },
                new PresentationAudioModel
                {
                    Id = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    Genre = GetTestGenreModel[0],
                    Name = "GROOOVY TRAP",
                    AuthorName = "Caslo",
                    PostedDate = new DateTime(2022, 6, 13)
                },
                new PresentationAudioModel
                {
                    Id = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    Genre = GetTestGenreModel[1],
                    Name = "Lazer Tag",
                    AuthorName = "SUPERARE",
                    PostedDate = new DateTime(2022, 6, 13)
                }
            };
        public List<GenreModel> GetTestGenreModel =>
           new List<GenreModel>()
           {
                new GenreModel { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Rock" },
                new GenreModel { Id = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"), Name = "Hip-hop" }
           };
        public List<Genre> GetTestGenreEntities =>
           new List<Genre>()
           {
                new Genre { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Rock" },
                new Genre { Id = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"), Name = "Hip-hop" }
           };
        public List<Audio> GetTestAudioEntities =>
            new List<Audio>()
            {
                new Audio
                {
                    Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "Make Funk",
                    AuthorName = "HoliznaCC0",
                    PostedDate = new DateTime(2022, 6, 18),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    Genre = GetTestGenreEntities[0]
                },
                new Audio
                {
                    Id = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "GROOOVY TRAP",
                    AuthorName = "Caslo",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    Genre = GetTestGenreEntities[0]
                },
                new Audio
                {
                    Id = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                    Name = "Lazer Tag",
                    AuthorName = "SUPERARE",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                    Genre = GetTestGenreEntities[1]
                }
            };
        #endregion
    }
}
