﻿using BLL.Models;
using BLL.Services;
using DL.Entities;
using DL.Enums;
using DL.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudioStorage.Tests.BusinessTests
{
    public class ReactionServiceTests
    {
        [Test]
        public async Task ReactionService_GetAll_ReturnsAllReactions()
        {
            //arrange
            var expected = GetTestReactionModels;
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(uow => uow.ReactionRepository.GetAllAsync())
                .ReturnsAsync(GetTestReactionEntities.AsEnumerable());
            var reactionService = new ReactionService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await reactionService.GetAllAsync();

            //assert
            actual.Should().BeEquivalentTo(expected);
        }
        [TestCase("b04aff1b-d794-47ad-80a1-1b090df80ae2")]
        [TestCase("d474c995-32fe-4176-ad1c-a0f77b466396")]
        public async Task ReactionService_GetByUserId_ReturnsReactions(Guid userId)
        {
            //arrange
            var expected = GetTestReactionModels.Where(r => r.UserId == userId);
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(uow => uow.ReactionRepository.GetByUserIdAsync(It.IsAny<Guid>()))
                .ReturnsAsync(GetTestReactionEntities
                    .Where(r => r.UserId == userId)
                    .AsEnumerable());
            var reactionService = new ReactionService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await reactionService.GetByUserIdAsync(userId);

            //assert
            actual.Should().BeEquivalentTo(expected);
        }
        [Test]
        public async Task ReactionService_AddAsync_AddsModel()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.ReactionRepository.AddAsync(It.IsAny<Reaction>()));

            var reactionService = new ReactionService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var reaction = GetTestReactionModels.First();

            //act
            await reactionService.AddAsync(reaction);

            //assert
            mockUnitOfWork.Verify(x => x.ReactionRepository.AddAsync(It.Is<Reaction>(r => r.Id == reaction.Id
                && r.UserId == reaction.UserId && r.AudioId == reaction.AudioId
                && r.Rate == reaction.Rate)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        #region TestData
        public List<ReactionModel> GetTestReactionModels =>
            new List<ReactionModel>
            {
                new ReactionModel
                {
                    Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                    AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    Rate = Rate.Like
                },
                new ReactionModel
                {
                    Id = new Guid("4ba7231c-04f3-4a02-988d-8c932068c6d2"),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    AudioId = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    Rate = Rate.Like
                }
            };
        public List<Reaction> GetTestReactionEntities =>
            new List<Reaction>
            {
                new Reaction
                {
                    Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                    AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("4ba7231c-04f3-4a02-988d-8c932068c6d2"),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    AudioId = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    Rate = Rate.Like
                }
            };
        #endregion
    }
}
