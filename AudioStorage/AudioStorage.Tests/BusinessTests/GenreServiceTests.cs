﻿using BLL.Exceptions;
using BLL.Models;
using BLL.Services;
using DL.Entities;
using DL.Interfaces;
using FluentAssertions;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AudioStorage.Tests.BusinessTests
{
    public class GenreServiceTests
    {
        [TestCase("c50e8047-454f-41be-9c6d-0ae44bb3aeb3")]
        [TestCase("adf99e0f-3d20-491a-bdcb-250a1a426b41")]
        public async Task GenreService_GetById_ReturnsGenreModel(Guid genreId)
        {
            //arrange
            var expected = GetTestGenreModels.First();
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(uow => uow.GenreRepository.GetByIdWithDetailsAsync(It.IsAny<Guid>()))
                .ReturnsAsync(GetTestGenreEntities.First());
            var genreService = new GenreService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await genreService.GetByIdAsync(genreId);

            //assert
            actual.Should().BeEquivalentTo(expected);
        }
        [Test]
        public async Task GenreService_GetAll_ReturnsAllGenres()
        {
            //arrange
            var expected = GetTestGenreModels;
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork
                .Setup(uow => uow.GenreRepository.GetAllWithDetailsAsync())
                .ReturnsAsync(GetTestGenreEntities.AsEnumerable());
            var genreService = new GenreService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            var actual = await genreService.GetAllAsync();

            //assert
            actual.Should().BeEquivalentTo(expected);
        }
        [Test]
        public async Task GenreService_AddAsync_AddsModel()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GenreRepository.AddAsync(It.IsAny<Genre>()));

            var genreService = new GenreService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var genre = GetTestGenreModels.First();

            //act
            await genreService.AddAsync(genre);

            //assert
            mockUnitOfWork.Verify(x => x.GenreRepository.AddAsync(It.Is<Genre>(g => g.Id == genre.Id
                && g.Name == genre.Name)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        [TestCase("c50e8047-454f-41be-9c6d-0ae44bb3aeb3")]
        [TestCase("adf99e0f-3d20-491a-bdcb-250a1a426b41")]
        public async Task GenreService_DeleteAsync_DeletesGenre(Guid genreId)
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GenreRepository.DeleteByIdAsync(It.IsAny<Guid>()));
            var genreService = new GenreService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());

            //act
            await genreService.DeleteAsync(genreId);

            //assert
            mockUnitOfWork.Verify(x => x.GenreRepository.DeleteByIdAsync(genreId), Times.Once());
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once());
        }
        [Test]
        public async Task GenreService_UpdateAsync_UpdatesModel()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GenreRepository.UpdateAsync(It.IsAny<Genre>()));

            var genreService = new GenreService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var genre = GetTestGenreModels.First();

            //act
            await genreService.UpdateAsync(genre);

            //assert
            mockUnitOfWork.Verify(x => x.GenreRepository.UpdateAsync(It.Is<Genre>(g => g.Id == genre.Id
                && g.Name == genre.Name)), Times.Once);
            mockUnitOfWork.Verify(x => x.SaveAsync(), Times.Once);
        }
        [Test]
        public async Task GenreService_AddAsync_ThrowsAudioStorageExceptionWithTheSameName()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GenreRepository.AddAsync(It.IsAny<Genre>()));
            mockUnitOfWork
                .Setup(m => m.GenreRepository.GetGenreByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(GetTestGenreEntities.First());

            var genreService = new GenreService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var genre = GetTestGenreModels.First();

            //act
            Func<Task> act = async () => await genreService.AddAsync(genre);

            //assert
            await act.Should().ThrowAsync<AudioStorageException>();
        }
        [Test]
        public async Task GenreService_UpdateAsync_ThrowsAudioStorageExceptionWithTheSameName()
        {
            //arrange
            var mockUnitOfWork = new Mock<IUnitOfWork>();
            mockUnitOfWork.Setup(m => m.GenreRepository.UpdateAsync(It.IsAny<Genre>()));
            mockUnitOfWork
                .Setup(m => m.GenreRepository.GetGenreByNameAsync(It.IsAny<string>()))
                .ReturnsAsync(GetTestGenreEntities.First());

            var genreService = new GenreService(mockUnitOfWork.Object, UnitTestHelper.CreateMapperProfile());
            var genre = GetTestGenreModels.First();

            //act
            Func<Task> act = async () => await genreService.UpdateAsync(genre);

            //assert
            await act.Should().ThrowAsync<AudioStorageException>();
        }
        #region TestData
        public List<GenreModel> GetTestGenreModels =>
            new List<GenreModel>
            {
                new GenreModel { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Rock" },
                new GenreModel { Id = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"), Name = "Hip-hop" }
            };
        public List<Genre> GetTestGenreEntities =>
            new List<Genre>
            {
                new Genre { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Rock" },
                new Genre { Id = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"), Name = "Hip-hop" }
            };
        #endregion
    }
}
