﻿using DL.Data;
using DL.Entities;
using DL.Enums;
using DL.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioStorage.Tests.DataTests
{
    public class ReactionRepositoryTests
    {
        [TestCase("2875ebbb-fa7c-4a11-9d16-2961b32cb09e")]
        [TestCase("4ba7231c-04f3-4a02-988d-8c932068c6d2")]
        public async Task ReactionRepository_GetByIdAsync_ReturnsSingleValue(Guid id)
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var reactionRepository = new ReactionRepository(context);
            var reaction = await reactionRepository.GetByIdAsync(id);
            var expected = ExpectedReactions.FirstOrDefault(x => x.Id == id);
            Assert.That(reaction, Is.EqualTo(expected).Using(new ReactionEqualityComparer()), message: "GetByIdAsync method works incorrect");
        }
        [Test]
        public async Task ReactionRepository_GetAllAsync_ReturnsAllValues()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var reactionRepository = new ReactionRepository(context);
            var reaction = await reactionRepository.GetAllAsync();
            Assert.That(reaction, Is.EqualTo(ExpectedReactions).Using(new ReactionEqualityComparer()), message: "GetAllAsync method works incorrect");
        }
        [Test]
        public async Task ReactionRepository_AddAsync_AddsValueToDatabase()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var reactionRepository = new ReactionRepository(context);
            var reaction = new Reaction
            {
                Id = new Guid("bc89ff70-a5ff-4d12-9158-89872f46d0a9"),
                UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                AudioId = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                Rate = Rate.Like
            };
            await reactionRepository.AddAsync(reaction);
            await context.SaveChangesAsync();
            Assert.That(context.Reactions.Count(), Is.EqualTo(3), message: "AddAsync method works incorrect");
        }
        [Test]
        public async Task ReactionRepository_DeleteByIdAsync_DeletesEntity()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var reactionRepository = new ReactionRepository(context);
            await reactionRepository.DeleteByIdAsync(new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"));
            await context.SaveChangesAsync();
            Assert.That(context.Reactions.Count(), Is.EqualTo(1), message: "DeleteByIdAsync works incorrect");
        }
        [Test]
        public async Task ReactionRepository_Update_UpdatesEntity()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());

            var reactionRepository = new ReactionRepository(context);
            var reaction = new Reaction
            {
                Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                Rate = Rate.Dislike
            };

            await reactionRepository.UpdateAsync(reaction);
            await context.SaveChangesAsync();
            Assert.That(reaction, Is.EqualTo(new Reaction
            {
                Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                Rate = Rate.Dislike
            }).Using(new ReactionEqualityComparer()), message: "Update method works incorrect");
        }
        [TestCase("b04aff1b-d794-47ad-80a1-1b090df80ae2")]
        [TestCase("d474c995-32fe-4176-ad1c-a0f77b466396")]
        public async Task ReactionRepository_GetByUserIdAsync_ReturnsSingleValue(Guid userId)
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var reactionRepository = new ReactionRepository(context);
            var reaction = await reactionRepository.GetByUserIdAsync(userId);
            var expected = ExpectedReactions.Where(x => x.UserId == userId);
            Assert.That(reaction, Is.EqualTo(expected).Using(new ReactionEqualityComparer()), message: "GetByUserIdAsync method works incorrect");
        }
        #region expected values
        private static IEnumerable<Reaction> ExpectedReactions =>
            new[]
            {
                new Reaction {
                    Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                    AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    Rate = Rate.Like },
                new Reaction {
                    Id = new Guid("4ba7231c-04f3-4a02-988d-8c932068c6d2"),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    AudioId = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    Rate = Rate.Dislike }
            };
        #endregion
    }
}
