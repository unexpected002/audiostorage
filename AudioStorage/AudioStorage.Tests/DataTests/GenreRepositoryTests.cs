﻿using DL.Data;
using DL.Entities;
using DL.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioStorage.Tests.DataTests
{
    public class GenreRepositoryTests
    {
        [TestCase("c50e8047-454f-41be-9c6d-0ae44bb3aeb3")]
        [TestCase("adf99e0f-3d20-491a-bdcb-250a1a426b41")]
        public async Task GenreRepository_GetByIdAsync_ReturnsSingleValue(Guid id)
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context);
            var genre = await genreRepository.GetByIdAsync(id);
            var expected = ExpectedGenres.FirstOrDefault(x => x.Id == id);
            Assert.That(genre, Is.EqualTo(expected).Using(new GenreEqualityComparer()), message: "GetByIdAsync method works incorrect");
        }
        [Test]
        public async Task GenreRepository_GetAllAsync_ReturnsAllValues()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context);
            var genre = await genreRepository.GetAllAsync();
            Assert.That(genre, Is.EqualTo(ExpectedGenres).Using(new GenreEqualityComparer()), message: "GetAllAsync method works incorrect");
        }
        [Test]
        public async Task GenreRepository_AddAsync_AddsValueToDatabase()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context);
            var genre = new Genre { Id = new Guid("8fcce438-7d38-4171-a261-2eed1219a3ec"), Name = "Electronic" };
            await genreRepository.AddAsync(genre);
            await context.SaveChangesAsync();
            Assert.That(context.Genres.Count(), Is.EqualTo(3), message: "AddAsync method works incorrect");
        }
        [Test]
        public async Task GenreRepository_DeleteByIdAsync_DeletesEntity()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context);
            await genreRepository.DeleteByIdAsync(new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"));
            await context.SaveChangesAsync();
            Assert.That(context.Genres.Count(), Is.EqualTo(1), message: "DeleteByIdAsync works incorrect");
        }
        [Test]
        public async Task GenreRepository_Update_UpdatesEntity()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context);
            var genre = new Genre { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Electronic" };
            await genreRepository.UpdateAsync(genre);
            await context.SaveChangesAsync();
            Assert.That(genre, Is.EqualTo(new Genre { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Electronic" })
                .Using(new GenreEqualityComparer()), message: "Update method works incorrect");
        }
        [Test]
        public async Task GenreRepository_GetByIdWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context);
            var genre = await genreRepository.GetByIdWithDetailsAsync(new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"));
            var expected = ExpectedGenres.FirstOrDefault(x => x.Id == new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"));
            Assert.That(genre,
                Is.EqualTo(expected).Using(new GenreEqualityComparer()), message: "GetByIdWithDetailsAsync method works incorrect");

            Assert.That(genre.Audio,
                Is.EqualTo(ExpectedAudio.Where(a => a.GenreId == expected.Id)).Using(new AudioEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");
        }

        [Test]
        public async Task GenreRepository_GetAllWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context);
            var genre = await genreRepository.GetAllWithDetailsAsync();
            Assert.That(genre,
                Is.EqualTo(ExpectedGenres).Using(new GenreEqualityComparer()), message: "GetByIdWithDetailsAsync method works incorrect");
            Assert.That(genre.SelectMany(g => g.Audio),
                Is.EqualTo(ExpectedAudio).Using(new AudioEqualityComparer()), message: "GetAllWithDetailsAsync method works incorrect");
        }
        [TestCase("Rock")]
        public async Task GenreRepository_GetGenreByNameAsync_ReturnsSingleValue(string genreName)
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var genreRepository = new GenreRepository(context);
            var genre = await genreRepository.GetGenreByNameAsync(genreName);
            var expected = ExpectedGenres.FirstOrDefault(x => x.Name == genreName);
            Assert.That(genre, Is.EqualTo(expected).Using(new GenreEqualityComparer()), message: "GetGenreByNameAsync method works incorrect");
        }
        #region expected values
        private static IEnumerable<Genre> ExpectedGenres =>
            new[]
            {
                new Genre { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Rock"},
                new Genre { Id = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"), Name = "Hip-hop" }
            };
        private static IEnumerable<Audio> ExpectedAudio =>
            new[]
            {
                new Audio
                {
                    Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "Make Funk",
                    AuthorName = "HoliznaCC0",
                    PostedDate = new DateTime(2022, 6, 18),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                },
                new Audio
                {
                    Id = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "GROOOVY TRAP",
                    AuthorName = "Caslo",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                },
                new Audio
                {
                    Id = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                    Name = "Lazer Tag",
                    AuthorName = "SUPERARE",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2")
                }
            };
        #endregion
    }
}
