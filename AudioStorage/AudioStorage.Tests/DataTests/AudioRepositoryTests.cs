﻿using DL.Data;
using DL.Entities;
using DL.Enums;
using DL.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioStorage.Tests.DataTests
{
    [TestFixture]
    public class AudioRepositoryTests
    {
        [TestCase("758bd178-dff9-42c5-8924-50bdfafdfa47")]
        [TestCase("91a2d7f3-bbf8-4001-a571-54f739b0c791")]
        public async Task AudioRepository_GetByIdAsync_ReturnsSingleValue(Guid id)
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var audioRepository = new AudioRepository(context);
            var audio = await audioRepository.GetByIdAsync(id);
            var expected = ExpectedAudio.FirstOrDefault(x => x.Id == id);
            Assert.That(audio, Is.EqualTo(expected).Using(new AudioEqualityComparer()), message: "GetByIdAsync method works incorrect");
        }
        [Test]
        public async Task AudioRepository_GetAllAsync_ReturnsAllValues()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var audioRepository = new AudioRepository(context);
            var audio = await audioRepository.GetAllAsync();
            Assert.That(audio, Is.EqualTo(ExpectedAudio).Using(new AudioEqualityComparer()), message: "GetAllAsync method works incorrect");
        }
        [Test]
        public async Task AudioRepository_AddAsync_AddsValueToDatabase()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var audioRepository = new AudioRepository(context);
            var audio = new Audio
            {
                Id = new Guid("6f56bd78-3e03-45ee-aacf-dbc0f8bce2f9"),
                GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                Name = "January 666",
                AuthorName = "John Lopker",
                PostedDate = new DateTime(2022, 6, 19),
                UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
            };
            await audioRepository.AddAsync(audio);
            await context.SaveChangesAsync();
            Assert.That(context.Audio.Count(), Is.EqualTo(4), message: "AddAsync method works incorrect");
        }
        [Test]
        public async Task AudioRepository_DeleteByIdAsync_DeletesEntity()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var audioRepository = new AudioRepository(context);
            await audioRepository.DeleteByIdAsync(new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"));
            await context.SaveChangesAsync();
            Assert.That(context.Audio.Count(), Is.EqualTo(2), message: "DeleteByIdAsync works incorrect");
        }
        [Test]
        public async Task AudioRepository_Update_UpdatesEntity()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());

            var audioRepository = new AudioRepository(context);
            var audio = new Audio
            {
                Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                Name = "Make Funk",
                AuthorName = "HoliznaCC0",
                PostedDate = new DateTime(2022, 6, 18),
                UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
            };

            await audioRepository.UpdateAsync(audio);
            await context.SaveChangesAsync();

            Assert.That(audio, Is.EqualTo(new Audio
            {
                Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                Name = "Make Funk",
                AuthorName = "HoliznaCC0",
                PostedDate = new DateTime(2022, 6, 18),
                UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
            }).Using(new AudioEqualityComparer()), message: "Update method works incorrect");
        }
        [Test]
        public async Task AudioRepository_GetByIdWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var audioRepository = new AudioRepository(context);
            var audio = await audioRepository.GetByIdWithDetailsAsync(new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"));
            var expected = ExpectedAudio.FirstOrDefault(x => x.Id == new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"));
            Assert.That(audio,
                Is.EqualTo(expected).Using(new AudioEqualityComparer()), message: "GetByIdWithDetailsAsync method works incorrect");

            Assert.That(audio.Reactions,
                Is.EqualTo(ExpectedReactions.Where(r => r.Id == expected.Id)).Using(new ReactionEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");

            Assert.That(audio.User,
                Is.EqualTo(ExpectedUsers.FirstOrDefault(u => u.Id == expected.UserId)).Using(new UserEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");
        }
        [Test]
        public async Task AudioRepository_GetAllWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var audioRepository = new AudioRepository(context);
            var audio = await audioRepository.GetAllWithDetailsAsync();
            Assert.That(audio,
                Is.EqualTo(ExpectedAudio).Using(new AudioEqualityComparer()), message: "GetAllWithDetailsAsync method works incorrect");
            Assert.That(audio.SelectMany(a => a.Reactions).OrderBy(r => r.Id),
                Is.EqualTo(ExpectedReactions).Using(new ReactionEqualityComparer()), message: "GetAllWithDetailsAsync method doesnt't return included entities");
            Assert.That(audio.Select(a => a.User).Distinct().OrderBy(u => u.Id),
                Is.EqualTo(ExpectedUsers).Using(new UserEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");
        }
        #region expected values
        private static IEnumerable<Audio> ExpectedAudio =>
            new[]
            {
                new Audio
                {
                    Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "Make Funk",
                    AuthorName = "HoliznaCC0",
                    PostedDate = new DateTime(2022, 6, 18),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                },
                new Audio
                {
                    Id = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "GROOOVY TRAP",
                    AuthorName = "Caslo",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                },
                new Audio
                {
                    Id = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                    Name = "Lazer Tag",
                    AuthorName = "SUPERARE",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2")
                }
            };
        private static IEnumerable<Reaction> ExpectedReactions =>
            new[]
            {
                new Reaction {
                    Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                    AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    Rate = Rate.Like },
                new Reaction {
                    Id = new Guid("4ba7231c-04f3-4a02-988d-8c932068c6d2"),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    AudioId = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    Rate = Rate.Dislike }
            };
        private static IEnumerable<User> ExpectedUsers =>
            new[]
            {
                new User { Id = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"), UserName = "Leonov Petro", Email = "admin@gmail.com", Birthdate = new DateTime(2001, 3, 20) },
                new User { Id = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"), UserName = "Valte Morte", Email = "user@gmail.com", Birthdate = new DateTime(1997, 1, 6) }
            };
        #endregion
    }
}
