﻿using DL.Data;
using DL.Entities;
using DL.Enums;
using DL.Repositories;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioStorage.Tests.DataTests
{
    public class UserRepositoryTests
    {
        [Test]
        public async Task UserRepository_GetByIdWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var userRepository = new UserRepository(context);
            var user = await userRepository.GetByIdWithDetailsAsync(new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"));
            var expected = ExpectedUsers.FirstOrDefault(x => x.Id == new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"));
            Assert.That(user,
                Is.EqualTo(expected).Using(new UserEqualityComparer()), message: "GetByIdWithDetailsAsync method works incorrect");
            Assert.That(user.Reactions,
                Is.EqualTo(ExpectedReactions.Where(r => r.UserId == expected.Id)).Using(new ReactionEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");
            var audio = ExpectedAudio.Select(a => a.UserId == expected.Id);
            Assert.That(user.Audio,
                Is.EqualTo(ExpectedAudio.Where(a => a.UserId == expected.Id)).Using(new AudioEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");
        }

        [Test]
        public async Task UserRepository_GetAllWithDetailsAsync_ReturnsWithIncludedEntities()
        {
            using var context = new ApplicationContext(UnitTestHelper.GetUnitTestDbOptions());
            var userRepository = new UserRepository(context);
            var user = await userRepository.GetAllWithDetailsAsync();
            Assert.That(user,
                Is.EqualTo(ExpectedUsers).Using(new UserEqualityComparer()), message: "GetAllWithDetailsAsync method works incorrect");
            Assert.That(user.SelectMany(u => u.Reactions).OrderBy(r => r.Id),
                Is.EqualTo(ExpectedReactions).Using(new ReactionEqualityComparer()), message: "GetAllWithDetailsAsync method doesnt't return included entities");
            Assert.That(user.SelectMany(u => u.Audio).OrderBy(a => a.Id),
                Is.EqualTo(ExpectedAudio).Using(new AudioEqualityComparer()), message: "GetByIdWithDetailsAsync method doesnt't return included entities");
        }
        #region expected values
        private static IEnumerable<User> ExpectedUsers =>
            new[]
            {
                new User { Id = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"), Email = "admin@gmail.com", UserName = "Leonov Petro" },
                new User { Id = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"), Email = "user@gmail.com", UserName = "Valte Morte" }
            };
        private static IEnumerable<Reaction> ExpectedReactions =>
            new[]
            {
                new Reaction
                {
                    Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                    AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("4ba7231c-04f3-4a02-988d-8c932068c6d2"),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    AudioId = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    Rate = Rate.Dislike
                }
            };
        private static IEnumerable<Audio> ExpectedAudio =>
            new[]
            {
                new Audio
                {
                    Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "Make Funk",
                    AuthorName = "HoliznaCC0",
                    PostedDate = new DateTime(2022, 6, 18),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                },
                new Audio
                {
                    Id = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "GROOOVY TRAP",
                    AuthorName = "Caslo",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                },
                new Audio
                {
                    Id = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                    Name = "Lazer Tag",
                    AuthorName = "SUPERARE",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2")
                }
            };
        #endregion
    }
}
