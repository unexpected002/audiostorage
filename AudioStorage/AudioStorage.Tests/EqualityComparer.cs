﻿using DL.Entities;
using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Text;

namespace AudioStorage.Tests
{
    internal class AudioEqualityComparer : IEqualityComparer<Audio>
    {
        public bool Equals([AllowNull] Audio x, [AllowNull] Audio y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id
                && x.GenreId == y.GenreId
                && x.Name == y.Name
                && x.AuthorName == y.AuthorName
                && x.PostedDate == y.PostedDate
                && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] Audio obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class GenreEqualityComparer : IEqualityComparer<Genre>
    {
        public bool Equals([AllowNull] Genre x, [AllowNull] Genre y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id
                && x.Name == y.Name;
        }

        public int GetHashCode([DisallowNull] Genre obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class ReactionEqualityComparer : IEqualityComparer<Reaction>
    {
        public bool Equals([AllowNull] Reaction x, [AllowNull] Reaction y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id
                && x.AudioId == y.AudioId
                && x.UserId == y.UserId;
        }

        public int GetHashCode([DisallowNull] Reaction obj)
        {
            return obj.GetHashCode();
        }
    }
    internal class UserEqualityComparer : IEqualityComparer<User>
    {
        public bool Equals([AllowNull] User x, [AllowNull] User y)
        {
            if (x == null && y == null)
                return true;
            if (x == null || y == null)
                return false;
            return x.Id == y.Id
                && x.Email == y.Email
                && x.UserName == y.UserName
                && x.PasswordHash == y.PasswordHash;
        }

        public int GetHashCode([DisallowNull] User obj)
        {
            return obj.GetHashCode();
        }
    }
}