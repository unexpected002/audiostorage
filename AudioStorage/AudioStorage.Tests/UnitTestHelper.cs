﻿using AutoMapper;
using BLL;
using DL.Data;
using DL.Entities;
using DL.Enums;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace AudioStorage.Tests
{
    internal static class UnitTestHelper
    {
        public static DbContextOptions<ApplicationContext> GetUnitTestDbOptions()
        {
            var options = new DbContextOptionsBuilder<ApplicationContext>()
                .UseInMemoryDatabase(Guid.NewGuid().ToString())
                .Options;

            using (var context = new ApplicationContext(options))
            {
                SeedData(context);
            }

            return options;
        }

        public static IMapper CreateMapperProfile()
        {
            var myProfile = new AutomapperProfile();
            var configuration = new MapperConfiguration(cfg => cfg.AddProfile(myProfile));

            return new Mapper(configuration);
        }


        public static void SeedData(ApplicationContext context)
        {
            context.Genres.AddRange(
                new Genre { Id = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"), Name = "Rock" },
                new Genre { Id = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"), Name = "Hip-hop" });
            context.Users.AddRange(
                new User 
                { 
                    Id = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"), 
                    UserName = "Leonov Petro", 
                    Email = "admin@gmail.com", 
                    Birthdate = new DateTime(2001, 3, 20),
                    ConcurrencyStamp = "bcd4229e-ca9c-4ddc-a035-a5a7a43a4d37"
                },
                new User 
                { 
                    Id = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"), 
                    Email = "user@gmail.com", 
                    UserName = "Valte Morte", 
                    Birthdate = new DateTime(1997, 1, 6),
                    ConcurrencyStamp = "c3a0d7d0-cb44-4866-b6ec-18e4caa48b80"
                });
            context.Audio.AddRange(
                new Audio
                {
                    Id = new Guid("758bd178-dff9-42c5-8924-50bdfafdfa47"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "Make Funk",
                    AuthorName = "HoliznaCC0",
                    PostedDate = new DateTime(2022, 6, 18),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                },
                new Audio
                {
                    Id = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    GenreId = new Guid("c50e8047-454f-41be-9c6d-0ae44bb3aeb3"),
                    Name = "GROOOVY TRAP",
                    AuthorName = "Caslo",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396")
                },
                new Audio
                {
                    Id = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    GenreId = new Guid("adf99e0f-3d20-491a-bdcb-250a1a426b41"),
                    Name = "Lazer Tag",
                    AuthorName = "SUPERARE",
                    PostedDate = new DateTime(2022, 6, 13),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2")
                });
            context.Reactions.AddRange(
                new Reaction
                {
                    Id = new Guid("2875ebbb-fa7c-4a11-9d16-2961b32cb09e"),
                    UserId = new Guid("b04aff1b-d794-47ad-80a1-1b090df80ae2"),
                    AudioId = new Guid("91a2d7f3-bbf8-4001-a571-54f739b0c791"),
                    Rate = Rate.Like
                },
                new Reaction
                {
                    Id = new Guid("4ba7231c-04f3-4a02-988d-8c932068c6d2"),
                    UserId = new Guid("d474c995-32fe-4176-ad1c-a0f77b466396"),
                    AudioId = new Guid("e9e0c39a-f09e-4194-8ac5-a0a9772e4eea"),
                    Rate = Rate.Like
                });
            context.SaveChanges();
        }
    }
}
