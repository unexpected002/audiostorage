import {NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {appRoutingModule} from './app.routing';

import {AppComponent} from './app.component';
import {MatCardModule} from "@angular/material/card";
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {OverviewComponent} from './components/overview/overview.component';
import {RouterModule} from "@angular/router";
import {LoginComponent} from './components/login/login.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {PublishedAudioComponent} from './components/published.audio/published.audio.component';
import {LikedAudioComponent} from './components/liked.audio/liked.audio.component';
import {JwtInterceptor} from "@app/_helpers/jwt.interceptor";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatTableModule} from "@angular/material/table";
import {MatInputModule} from "@angular/material/input";
import {GenresComponent} from './components/genres/genres.component';
import {UsersComponent} from './components/users/users.component';
import {AudioComponent} from './components/audio/audio.component';
import {UserProfileComponent} from './components/user.profile/user.profile.component';
import {RegisterComponent} from './components/register/register.component';
import {MatDatepickerModule} from "@angular/material/datepicker";
import {MatNativeDateModule} from "@angular/material/core";
import {AlertModule} from "@app/_alert/alert.module";
import {MatAutocompleteModule} from "@angular/material/autocomplete";

@NgModule({
    imports: [
        BrowserAnimationsModule,
        BrowserModule,
        HttpClientModule,
        MatCardModule,
        MatButtonModule,
        MatIconModule,
        RouterModule,
        appRoutingModule,
        ReactiveFormsModule,
        MatFormFieldModule,
        MatPaginatorModule,
        MatTableModule,
        MatInputModule,
        FormsModule,
        MatDatepickerModule,
        MatNativeDateModule,
        AlertModule,
        MatAutocompleteModule
    ],
  declarations: [
    AppComponent,
    OverviewComponent,
    LoginComponent,
    PublishedAudioComponent,
    LikedAudioComponent,
    GenresComponent,
    UsersComponent,
    AudioComponent,
    UserProfileComponent,
    RegisterComponent,
  ],

  providers: [
    {provide: HTTP_INTERCEPTORS, useClass: JwtInterceptor, multi: true}
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
