import {Routes, RouterModule} from '@angular/router';

import {OverviewComponent} from './components/overview/overview.component';
import {LoginComponent} from "@app/components/login/login.component";
import {PublishedAudioComponent} from "@app/components/published.audio/published.audio.component";
import {LikedAudioComponent} from "@app/components/liked.audio/liked.audio.component";
import {GenresComponent} from "@app/components/genres/genres.component";
import {AudioComponent} from "@app/components/audio/audio.component";
import {UserProfileComponent} from "@app/components/user.profile/user.profile.component";
import {RegisterComponent} from "@app/components/register/register.component";
import {UsersComponent} from "@app/components/users/users.component";

const routes: Routes = [
  {
    path: 'overview',
    component: OverviewComponent,
  },
  {
    path: 'audio',
    component: AudioComponent,
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: 'register',
    component: RegisterComponent
  },
  {
    path: 'published-audio',
    component: PublishedAudioComponent
  },
  {
    path: 'liked-audio',
    component: LikedAudioComponent
  },
  {
    path: 'genres',
    component: GenresComponent
  },
  {
    path: 'users',
    component: UsersComponent
  },
  {
    path: 'user-profile',
    component: UserProfileComponent
  },
  {
    path: '**',
    redirectTo: 'overview'
  }
];

export const appRoutingModule = RouterModule.forRoot(routes);
