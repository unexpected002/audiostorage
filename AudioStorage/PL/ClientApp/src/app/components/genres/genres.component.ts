import {Component, OnInit} from '@angular/core';
import {Genre} from "@app/_models/genre";
import {environment} from "@environments/environment";
import {HttpClient} from "@angular/common/http";
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {AlertService} from "@app/_alert/alert.service";

@Component({
  selector: 'app-genres',
  templateUrl: './genres.component.html',
  styleUrls: ['./genres.component.css'],
  providers: [AlertService]
})
export class GenresComponent implements OnInit {
  genres: Genre[] = [];
  genre: Genre = new Genre();
  counter!: number;
  tableMode: boolean = true;
  genreErrors!: GenreError;

  submitted: boolean = false;
  preSubmitted: boolean = false;
  inputForm!: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private http: HttpClient,
    protected alertService: AlertService
  ) {
    this.inputForm = this.formBuilder.group({
      name: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.loadGenres();
  }

  loadGenres(): void {
    this.http.get<Genre[]>(environment.apiUrl + "/genres").subscribe(
      genres => {
        this.genres = genres;
      });
  }

  get f(): any {
    return this.inputForm.controls;
  }

  get incrementCounter(): number {
    return this.counter++;
  }

  setCounter(): void {
    this.counter = 1;
  }

  // сохранение данных
  save() {
    this.submitted = true;
    this.preSubmitted = false;

    if (this.inputForm.invalid) {
      return;
    }

    this.submitted = false;
    this.preSubmitted = true;

    if (!this.genre.id) {
      this.http.post(environment.apiUrl + `/genres`, this.genre)
        .subscribe(data => {
          this.loadGenres();
          this.cancel();
        }, err => {
          switch (err.status) {
            case 422: {
              this.genreErrors = err.error;
              break;
            }
            case 409: {
              this.alertService.warn(err.error);
              break;
            }
            case 406: {
              this.alertService.error(err.error);
              break;
            }
            case 400: {
              this.alertService.error('Something went wrong!');
              break;
            }
          }
        });
    } else {
      this.http.put(environment.apiUrl + `/genres/${this.genre?.id}`, this.genre)
        .subscribe(data => {
          this.loadGenres();
          this.cancel();
        }, err => {
          switch (err.status) {
            case 422: {
              this.genreErrors = err.error;
              break;
            }
            case 409: {
              this.alertService.warn(err.error);
              break;
            }
            case 406: {
              this.alertService.error(err.error);
              break;
            }
            case 400: {
              this.alertService.error('Something went wrong!');
              break;
            }
          }
        });
    }
  }

  editProduct(g: Genre) {
    this.genre = new Genre();
    this.genre.id = g.id;
    this.genre.name = g.name
    this.tableMode = false;
  }

  cancel() {
    this.genre = new Genre();
    this.submitted = false;
    this.preSubmitted = false;
    this.tableMode = true;
  }

  delete(genre: Genre) {
    this.http.delete(environment.apiUrl + `/genres/${genre?.id}`)
      .subscribe(data => this.loadGenres());
  }

  add() {
    this.cancel();
    this.tableMode = false;
  }

  get isGenreErrors(): boolean {
    return this.genreErrors?.Name?.length > 0;
  }
}

interface GenreError {
  Name: Array<string>
}
