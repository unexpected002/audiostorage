import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LikedAudioComponent } from './liked.audio.component';

describe('Liked.AudioComponent', () => {
  let component: LikedAudioComponent;
  let fixture: ComponentFixture<LikedAudioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LikedAudioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LikedAudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
