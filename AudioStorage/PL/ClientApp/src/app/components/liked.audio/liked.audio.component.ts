import {Component, OnInit, ViewChild} from '@angular/core';
import {Audio} from "@app/_models/audio";
import {User} from "@app/_models/user";
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "@app/_services/authentication.service";
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import {AudioView} from "@app/_models/audio.view";
import {AudioService} from "@app/_services/audio.service";
import {Guid} from "guid-typescript";

@Component({
  selector: 'app-liked.audio',
  templateUrl: './liked.audio.component.html',
  styleUrls: ['./liked.audio.component.css', '../../styles/user-audio.css'],
  providers: [AudioService]
})
export class LikedAudioComponent implements OnInit {
  likedAudio: Audio[] = [];
  likedAudioView: AudioView[] = [];
  currentUser: User | null = null;
  dataSource!: MatTableDataSource<AudioView>;

  pageSizeOptions: number[] = [5, 10, 25, 50];

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService,
    private audioService: AudioService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    if(this.currentUser != null) {
      this.audioService.getLikedAudioByUser().subscribe(
        audio => {
          this.likedAudio = audio;
          this.likedAudio.forEach(pa => this.likedAudioView.push(new AudioView(pa.id, pa.name, pa.genre.name, pa.authorName, pa.postedDate)));
          this.dataSource = new MatTableDataSource<AudioView>(this.likedAudioView);
          this.dataSource.paginator = this.paginator;
        });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  displayedColumns: string[] = ['name', 'genreName', 'authorName', 'postedDate'];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
}
