import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {first} from "rxjs";
import {AuthenticationService} from "@app/_services/authentication.service";
import {AlertService} from "@app/_alert/alert.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  providers: [AlertService]
})
export class LoginComponent implements OnInit {

  loginForm!: FormGroup;
  returnUrl!: string;
  loading: boolean = false;
  submitted: boolean = false;
  preSubmitted: boolean = false;
  authenticateErrors!: AuthenticateError;

  constructor(
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService,
    protected alertService: AlertService
  ) {
  }

  ngOnInit(): void {
    this.loginForm = this.formBuilder.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern("^(?:[\\w]){3,10}@(?:[\\w\\-]+)(?:(?:\\.(?:\\w){1,3}))$")])],
      password: ['', Validators.required]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f(): any { return this.loginForm.controls; }

  onSubmit() {
    this.submitted = false;
    this.preSubmitted = true;

    // stop here if form is invalid
    if (this.loginForm.invalid) {
      return;
    }

    this.submitted = true;
    this.preSubmitted = false;

    this.loading = true;
    this.authenticationService.login(this.f['email'].value, this.f['password'].value)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        err => {
          switch (err.status)
          {
            case 422: {
              this.authenticateErrors = err.error;
              break;
            }
            case 406: {
              this.alertService.error(err.error);
              break;
            }
            case 404: {
              this.alertService.error('Incorrect email or password!');
              break;
            }
            case 400: {
              this.alertService.error('Something went wrong!');
              break;
            }
          }
          this.loading = false;
        });
  }

  get isEmailErrors(): boolean {
    return this.authenticateErrors?.Email?.length > 0;
  }

  get isPasswordErrors(): boolean {
    return this.authenticateErrors?.Password?.length > 0;
  }
}

interface AuthenticateError {
  Email: Array<string>,
  Password: Array<string>
}
