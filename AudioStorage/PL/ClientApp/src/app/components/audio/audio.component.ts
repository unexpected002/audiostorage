import {Component, OnInit} from '@angular/core';
import {Audio} from "@app/_models/audio";
import {AuthenticationService} from "@app/_services/authentication.service";
import {User} from "@app/_models/user";
import {environment} from "@environments/environment";
import {HttpClient} from "@angular/common/http";
import {AudioService} from "@app/_services/audio.service";

@Component({
  selector: 'app-audio',
  templateUrl: './audio.component.html',
  styleUrls: ['./audio.component.css', '../../styles/audio.css'],
  providers: [AudioService]
})
export class AudioComponent implements OnInit {
  audio!: Audio[];
  currentUser: User | null = null;
  searchSubstring: string = '';

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService,
    private audioService: AudioService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    this.http.get<Audio[]>(environment.apiUrl + `/audio`).subscribe(
      audio => {
        this.audio = audio;
      }
    );
  }

  get AudioService(): AudioService {
    return this.audioService;
  }

  get AudioWithFilter(): Audio[] {
    if(this.searchSubstring.length > 0) {
      return this.audio.filter(a => a.name.toLowerCase().includes(this.searchSubstring.toLowerCase())
        || a.genre.name.toLowerCase().includes(this.searchSubstring.toLowerCase())
        || a.authorName.toLowerCase().includes(this.searchSubstring.toLowerCase()))
    } else {
      return this.audio;
    }
  }
}
