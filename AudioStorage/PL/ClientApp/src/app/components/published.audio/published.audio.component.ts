import {Component, OnInit, ViewChild} from '@angular/core';
import {User} from "@app/_models/user";
import {HttpClient} from "@angular/common/http";
import {AuthenticationService} from "@app/_services/authentication.service";
import {Audio} from "@app/_models/audio";
import {MatTableDataSource} from "@angular/material/table";
import {MatPaginator} from "@angular/material/paginator";
import {AudioView} from "@app/_models/audio.view";
import {AudioService} from "@app/_services/audio.service";
import {Guid} from "guid-typescript";

@Component({
  selector: 'app-published.audio',
  templateUrl: './published.audio.component.html',
  styleUrls: ['./published.audio.component.css', '../../styles/user-audio.css'],
  providers: [AudioService]
})
export class PublishedAudioComponent implements OnInit {
  publishedAudio: Audio[] = [];
  publishedAudioView: AudioView[] = [];
  currentUser: User | null = null;
  dataSource!: MatTableDataSource<AudioView>;

  pageSizeOptions: number[] = [5, 10, 25, 50];

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService,
    private audioService: AudioService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    if(this.currentUser != null) {
      this.audioService.getPublishedAudioByUser().subscribe(
        audio => {
          this.publishedAudio = audio;
          this.publishedAudio.forEach(pa => this.publishedAudioView.push(new AudioView(pa.id, pa.name, pa.genre.name, pa.authorName, pa.postedDate)));
          this.dataSource = new MatTableDataSource<AudioView>(this.publishedAudioView);
          this.dataSource.paginator = this.paginator;
        });
    }
  }

  applyFilter(event: Event) {
    const filterValue = (event.target as HTMLInputElement).value;
    this.dataSource.filter = filterValue.trim().toLowerCase();

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  displayedColumns: string[] = ['name', 'genreName', 'authorName', 'postedDate'];

  @ViewChild(MatPaginator) paginator!: MatPaginator;
}
