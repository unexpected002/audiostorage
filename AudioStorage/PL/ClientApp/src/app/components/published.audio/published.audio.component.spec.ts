import { ComponentFixture, TestBed } from '@angular/core/testing';

import { PublishedAudioComponent } from './published.audio.component';

describe('Published.AudioComponent', () => {
  let component: PublishedAudioComponent;
  let fixture: ComponentFixture<PublishedAudioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ PublishedAudioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(PublishedAudioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
