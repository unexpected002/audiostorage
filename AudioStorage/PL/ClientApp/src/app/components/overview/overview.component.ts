import {Component, OnInit} from '@angular/core';
import {Genre} from "@app/_models/genre";
import {HttpClient} from "@angular/common/http";
import {Audio} from "@app/_models/audio";
import {Guid} from "guid-typescript";
import {User} from "@app/_models/user";
import {AuthenticationService} from "@app/_services/authentication.service";
import {AudioService} from "@app/_services/audio.service";

@Component({
  selector: './overview-component',
  templateUrl: './overview.component.html',
  styleUrls: ['./overview.component.css', '../../styles/audio.css'],
  providers: [AudioService]
})
export class OverviewComponent implements OnInit {
  genres: Genre[] = [];
  audioByGenres!: Map<Guid, Audio[]>;
  currentUser: User | null = null;

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService,
    private audioService: AudioService,
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
  }

  ngOnInit(): void {
    this.audioService.getGenres().subscribe(
      genres => {
        this.genres = genres;
        this.audioByGenres = new Map<Guid, Audio[]>();
        for (let genre of this.genres) {
          this.audioService.getAudioByGenre(genre.id).subscribe(
            audio => {
              console.log(audio);
              this.audioByGenres.set(genre.id, audio);
            });
        }
      });
  }

  audioByGenre(genreId: Guid): Audio[] | undefined {
    return this.audioByGenres.get(genreId);
  }

  get AudioService(): AudioService {
    return this.audioService;
  }
}
