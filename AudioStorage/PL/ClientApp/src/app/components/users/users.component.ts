import { Component, OnInit } from '@angular/core';
import {Genre} from "@app/_models/genre";
import {User} from "@app/_models/user";
import {environment} from "@environments/environment";
import {HttpClient} from "@angular/common/http";

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.css']
})
export class UsersComponent implements OnInit {
  users: User[] = [];
  user: User = new User();
  counter!: number;

  constructor(
    private http: HttpClient
  ) { }

  ngOnInit(): void {
    this.loadUsers();
  }

  loadUsers(): void {
    this.http.get<User[]>(environment.apiUrl + "/users").subscribe(
      users => {
        this.users = users;
      });
  }

  get incrementCounter(): number {
    return this.counter++;
  }

  setCounter(): void {
    this.counter = 1;
  }

  delete(user: User) {
    this.http.delete(environment.apiUrl + `/users/${user?.id}`)
      .subscribe(data => this.loadUsers());
  }
}
