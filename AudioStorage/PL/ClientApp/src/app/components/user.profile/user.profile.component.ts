import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {HttpClient} from "@angular/common/http";
import {environment} from "@environments/environment";
import {ActivatedRoute, Router} from "@angular/router";
import {AlertService} from "@app/_alert/alert.service";

@Component({
  selector: 'app-user.profile',
  templateUrl: './user.profile.component.html',
  styleUrls: ['./user.profile.component.css', '../../styles/icons.css', '../../styles/forms.css'],
  providers: [AlertService]
})
export class UserProfileComponent implements OnInit {
  changePasswordForm!: FormGroup;
  hideoP: boolean = true;
  hideNP: boolean = true;
  submitted: boolean = false;
  preSubmitted: boolean = false;
  changePasswordError!: ChangePasswordError;
  returnUrl!: string;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    protected alertService: AlertService
  ) {
  }

  ngOnInit(): void {
    this.changePasswordForm = this.formBuilder.group({
      password: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
      newPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
      repeatPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  get f(): any {
    return this.changePasswordForm.controls;
  }

  onSubmit() {
    this.submitted = false;
    this.preSubmitted = true;

    if (this.changePasswordForm.invalid) {
      return;
    }

    this.submitted = true;
    this.preSubmitted = false;

    if (!this.isCorrectRepeated) {
      return;
    }

    let oldPassword: string = this.f.password.value;
    let password: string = this.f.newPassword.value;

    console.log(`${environment.apiUrl}/account/change-password`);
    this.http.post(`${environment.apiUrl}/account/change-password`, {oldPassword, password})
      .subscribe(
        response => {
          this.router.navigate([this.returnUrl]);
        },
        err => {
          switch (err.status) {
            case 422: {
              this.changePasswordError = err.error;
              break;
            }
            case 409: {
              console.log(err.error);
              this.alertService.warn(err.error);
              break;
            }
            case 406: {
              this.alertService.error(err.error);
              break;
            }
            case 400: {
              this.alertService.error('Something went wrong!');
              break;
            }
          }
        });
  }

  get isCorrectRepeated(): boolean {
    return this.f.newPassword.value === this.f.repeatPassword.value;
  }

  get isPasswordErrors(): boolean {
    return this.changePasswordError?.Password?.length > 0;
  }
}

interface ChangePasswordError {
  Password: Array<string>
}
