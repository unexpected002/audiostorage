import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {ActivatedRoute, Router} from "@angular/router";
import {HttpClient} from "@angular/common/http";
import {environment} from "@environments/environment";
import {AlertService} from "@app/_alert/alert.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css', '../../styles/icons.css', '../../styles/forms.css'],
  providers: [AlertService]
})
export class RegisterComponent implements OnInit {

  registerForm!: FormGroup;
  hideoP: boolean = true;
  returnUrl!: string;
  submitted: boolean = false;
  preSubmitted: boolean = false;
  userModelErrors!: UserModelErrors;

  constructor(
    private http: HttpClient,
    private formBuilder: FormBuilder,
    private route: ActivatedRoute,
    private router: Router,
    protected alertService: AlertService
  ) {
  }

  ngOnInit(): void {
    this.registerForm = this.formBuilder.group({
      userName: ['', Validators.compose([Validators.required, Validators.pattern("^([A-Za-z]{3,}\\s[A-Za-z]{3,})$")])],
      email: ['', Validators.compose([Validators.required, Validators.pattern("^(?:[\\w]){3,20}@(?:[\\w\\-]+)(?:(?:\\.(?:\\w){2,3}))$")])],
      phone: ['+380', Validators.compose([Validators.required, Validators.pattern("^\\+380(?:\\d{9})$")])],
      birthdate: ['', Validators.required],
      password: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
      repeatPassword: ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])]
    });
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/login';
  }

  get f(): any {
    return this.registerForm.controls;
  }

  onSubmit() {
    this.submitted = false;
    this.preSubmitted = true;

    // stop here if form is invalid
    if (this.registerForm.invalid) {
      return;
    }

    this.submitted = true;
    this.preSubmitted = false;

    if (!this.isCorrectRepeated) {
      return;
    }
    let userName = this.f.userName.value;
    let email = this.f.email.value;
    let phone = this.f.phone.value;
    let birthdate = this.f.birthdate.value;
    let password = this.f.password.value;

    this.http.post(environment.apiUrl + "/account/register", {userName, email, phone, birthdate, password})
      .subscribe(
        data => {
          this.router.navigate([this.returnUrl]);
        },
        err => {
          switch (err.status) {
            case 422: {
              this.userModelErrors = err.error;
              break;
            }
            case 409: {
              this.alertService.warn(err.error);
              break;
            }
            case 406: {
              this.alertService.error(err.error);
              break;
            }
            case 400: {
              this.alertService.error('Something went wrong!');
              break;
            }
          }
          window.scroll(0,0);
        });
  }

  get isCorrectRepeated(): boolean {
    return this.f.password.value === this.f.repeatPassword.value;
  }

  get isUserNameErrors(): boolean {
    return this.userModelErrors?.UserName?.length > 0;
  }

  get isEmailErrors(): boolean {
    return this.userModelErrors?.Email?.length > 0;
  }

  get isPhoneErrors(): boolean {
    return this.userModelErrors?.Phone?.length > 0;
  }

  get isBirthdateErrors(): boolean {
    return this.userModelErrors?.Birthdate?.length > 0;
  }

  get isPasswordErrors(): boolean {
    return this.userModelErrors?.Password?.length > 0;
  }
}

interface UserModelErrors {
  UserName: Array<string>,
  Email: Array<string>,
  Phone: Array<string>,
  Birthdate: Array<string>,
  Password: Array<string>
}
