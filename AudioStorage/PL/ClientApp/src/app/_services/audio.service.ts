import {Injectable, OnInit} from "@angular/core";
import {Audio} from "@app/_models/audio";
import {Genre} from "@app/_models/genre";
import {Guid} from "guid-typescript";
import {HttpClient} from "@angular/common/http";
import {environment} from "@environments/environment";
import {Observable} from "rxjs";
import * as fileSaver from "file-saver";
import {Reaction} from "@app/_models/reaction";
import {AuthenticationService} from "@app/_services/authentication.service";
import {User} from "@app/_models/user";

@Injectable()
export class AudioService {
  reactions!: Reaction[];
  currentUser: User | null = null;

  constructor(
    private http: HttpClient,
    private authenticationService: AuthenticationService
  ) {
    this.authenticationService.currentUser.subscribe(x => this.currentUser = x);
    if(this.currentUser) {
      this.getReactionByUser();
    }
  }

  getGenres(): Observable<Genre[]> {
    return this.http.get<Genre[]>(environment.apiUrl + "/genres");
  }

  getAudioByGenre(genreId: Guid): Observable<Audio[]> {
    return this.http.get<Audio[]>(environment.apiUrl + `/overview/${genreId}`);
  }

  getPublishedAudioByUser(): Observable<Audio[]> {
    return this.http.get<Audio[]>(environment.apiUrl + `/overview/published-audio`);
  }

  getLikedAudioByUser(): Observable<Audio[]> {
    return this.http.get<Audio[]>(environment.apiUrl + `/overview/liked-audio`);
  }

  toLike(audioId: Guid): void {
    alert("You have liked audio");
  }

  toDownload(audioId: Guid): void {
    this.http.get(environment.apiUrl + `/audio/download/${audioId}`, {responseType: 'blob', observe: 'response'})
      .subscribe((response: any) => {
        let fileName: string = response.headers.get('content-disposition').split('filename=')[1].split(';')[0];
        fileName = fileName.replace(/"/g, '');
        let blob: any = new Blob([response.body], {type: 'audio/mpeg'});
        fileSaver.saveAs(blob, `${fileName}.mp3`);
      });
  }

  isLiked(audioId: Guid): boolean {
    return this.reactions?.some(r => r.userId === this.currentUser?.id && r.audioId === audioId);
  }

  getReactionByUser(): void {
    this.http.get<Reaction[]>(environment.apiUrl + `/audio/reactions`).subscribe(
      reactions => {
        this.reactions = reactions;
      });
  }
}
