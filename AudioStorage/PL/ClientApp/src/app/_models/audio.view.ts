import {Guid} from "guid-typescript";

export class AudioView {
  id: Guid;
  name: string;
  genreName: string;
  authorName: string;
  postedDate: Date;
  constructor(id: Guid, name: string, genreName: string, authorName: string, postedDate: Date) {
    this.id = id;
    this.genreName = genreName;
    this.name = name;
    this.authorName = authorName;
    this.postedDate = postedDate;
  }
}
