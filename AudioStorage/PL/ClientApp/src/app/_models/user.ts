import { Guid } from "guid-typescript";

export class User {
  id!: Guid;
  userName!: string;
  email!: string;
  phone!: string;
  birthdate!: Date;
  role!: string;
  token!: string;
}
