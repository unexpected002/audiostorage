import {Guid} from "guid-typescript";
import {Rate} from "@app/_models/Enums/Rate";

export interface Reaction {
  id: Guid;
  userId: Guid;
  audioId: Guid;
  rate: Rate;
}
