import { Guid } from "guid-typescript";

export class Genre {
  id!: Guid;
  name!: string;
}
