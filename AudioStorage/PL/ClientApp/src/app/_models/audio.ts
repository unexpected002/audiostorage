import {Guid} from "guid-typescript";
import {Genre} from "@app/_models/genre";

export interface Audio {
  id: Guid;
  genre: Genre;
  name: string;
  authorName: string;
  postedDate: Date;
}
