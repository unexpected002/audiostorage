﻿using AutoMapper;
using BLL.Interfaces;
using BLL.Models;
using BLL.Models.Audio;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PL.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "User, Administrator")]
    public class AudioController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly IAudioService _audioService;
        private readonly IReactionService _reactionService;

        public AudioController(IMapper mapper, IAudioService audioService, IReactionService reactionService)
        {
            _mapper = mapper;
            _audioService = audioService;
            _reactionService = reactionService;
        }

        /// <summary>
        /// Gets all audio.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<PresentationAudioModel>>> GetAll()
        {
            var audio = await _audioService.GetAllAsync();
            return Ok(audio);
        }
        /// <summary>
        /// Gets the audio by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<PresentationAudioModel>> GetAudioById([FromRoute] Guid id)
        {
            var audio = await _audioService.GetByIdAsync(id);
            return Ok(audio);
        }
        /// <summary>
        /// Adds the audio.
        /// </summary>
        /// <param name="model">The creation audio model.</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AudioModelValidationFilter))]
        public async Task<ActionResult> AddAudio([FromBody] CreationAudioModel model)
        {
            var audio = _mapper.Map<AudioModel>(model);
            audio.PostedDate = DateTime.Now;
            audio.UserId = Guid.Parse(Request.HttpContext.User.Identity.Name);

            await _audioService.AddAsync(audio);
            return Ok();
        }
        /// <summary>
        /// Updates the audio.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The update audio model.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ServiceFilter(typeof(AudioModelValidationFilter))]
        public async Task<ActionResult> UpdateAudio([FromRoute] Guid id, [FromBody] UpdateAudioModel model)
        {

            var audio = _mapper.Map<AudioModel>(model);
            audio.PostedDate = DateTime.Now;
            audio.UserId = Guid.Parse(Request.HttpContext.User.Identity.Name);

            await _audioService.UpdateAsync(audio);
            return Ok();

        }
        /// <summary>
        /// Deteles the audio.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult> DeteleAudio([FromRoute] Guid id)
        {

            await _audioService.DeleteAsync(id);
            return Ok();

        }
        /// <summary>
        /// Gets the audio reactions by user identifier.
        /// </summary>
        /// <returns></returns>
        [HttpGet("reactions")]
        public async Task<ActionResult> GetAudioReactionsByUserId()
        {
            var userId = Guid.Parse(Request.HttpContext.User.Identity.Name);
            var reactions = await _reactionService.GetByUserIdAsync(userId);

            return Ok(reactions);

        }
        /// <summary>
        /// Adds the audio reactions.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <param name="model">The reaction model.</param>
        /// <returns></returns>
        [HttpPost("reactions/{id}")]
        public async Task<ActionResult> AddAudioReactions([FromRoute] Guid id, [FromBody] ReactionModel model)
        {
            await _reactionService.AddAsync(model);
            return Ok();
        }
        /// <summary>
        /// Downloads the audio by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("download/{id}")]
        [AllowAnonymous]
        public async Task<ActionResult> DownloadAudio([FromRoute] Guid id)
        {
            var audioFileModel = await _audioService.DownloadAsync(id);
            return File(audioFileModel.AudioData, "audio/mpeg", audioFileModel.AudioName);
        }
    }
}