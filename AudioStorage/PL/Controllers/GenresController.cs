﻿using BLL.Interfaces;
using BLL.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PL.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "Administrator")]
    public class GenresController : ControllerBase
    {
        private readonly IGenreService _genreService;

        public GenresController(IGenreService genreService)
        {
            _genreService = genreService;
        }

        /// <summary>
        /// Gets all genres.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<GenreModel>>> GetAll()
        {

            var genres = await _genreService.GetAllAsync();
            return Ok(genres);
        }
        /// <summary>
        /// Gets the genre by identifier.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<GenreModel>> GetGenreById([FromRoute] Guid id)
        {
            var genre = await _genreService.GetByIdAsync(id);
            return Ok(genre);
        }
        /// <summary>
        /// Adds the genre.
        /// </summary>
        /// <param name="model">The model.</param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(GenreModelValidationFilter))]
        public async Task<ActionResult<IEnumerable<GenreModel>>> AddGenre([FromBody] GenreModel model)
        {
            await _genreService.AddAsync(model);
            return Ok();
        }
        /// <summary>
        /// Updates the genre.
        /// </summary>
        /// <param name="genreId">The genre identifier.</param>
        /// <param name="model">The genre model.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ServiceFilter(typeof(GenreModelValidationFilter))]
        public async Task<ActionResult<IEnumerable<GenreModel>>> UpdateGenre([FromRoute] Guid genreId, GenreModel model)
        {
            await _genreService.UpdateAsync(model);
            return Ok();
        }
        /// <summary>
        /// Deteles the genre.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        public async Task<ActionResult<IEnumerable<GenreModel>>> DeteleGenre([FromRoute] Guid id)
        {

            await _genreService.DeleteAsync(id);
            return Ok();
        }
    }
}
