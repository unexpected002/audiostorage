﻿using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PL.Filters;
using System;
using System.Threading.Tasks;

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [AllowAnonymous]
    public class AccountController : ControllerBase
    {
        private readonly IUserService _userService;

        public AccountController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Registers users.
        /// </summary>
        /// <param name="model">The register model.</param>
        /// <returns></returns>
        [HttpPost("register")]
        [ServiceFilter(typeof(UserModelValidationFilter))]
        public async Task<ActionResult> Register([FromBody] RegistrationModel model)
        {
            await _userService.AddAsync(model);
            return Ok();
        }
        /// <summary>
        /// Authenticates users.
        /// </summary>
        /// <param name="model">The authenticate model.</param>
        /// <returns></returns>
        [HttpPost("authenticate")]
        [ServiceFilter(typeof(UserModelValidationFilter))]
        public async Task<ActionResult<SessionModel>> Authenticate([FromBody] AuthenticateModel model)
        {
            var user = await _userService.Authenticate(model);

            if (user == null)
            {
                return NotFound();
            }

            return Ok(user);
        }
        /// <summary>
        /// Changes password by user.
        /// </summary>
        /// <param name="model">The new password model.</param>
        /// <returns></returns>
        [HttpPost("change-password")]
        [ServiceFilter(typeof(UserModelValidationFilter))]
        [Authorize]
        public async Task<ActionResult> ChangePassword([FromBody] NewPasswordModel model)
        {

            var userId = Guid.Parse(Request.HttpContext.User.Identity.Name);

            await _userService.ChangePassword(userId, model);
            return Ok();
        }
    }
}