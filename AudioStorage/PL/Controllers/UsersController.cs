﻿using BLL.Interfaces;
using BLL.Models;
using BLL.Models.User;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PL.Filters;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class UsersController : ControllerBase
    {
        private readonly IUserService _userService;

        public UsersController(IUserService userService)
        {
            _userService = userService;
        }

        /// <summary>
        /// Gets all users.
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<IEnumerable<UserModel>>> GetAll()
        {
            var users = await _userService.GetAllAsync();
            return Ok(users);
        }
        /// <summary>
        /// Gets the user by identifier.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [ServiceFilter(typeof(AccessFilter))]
        public async Task<ActionResult<UserModel>> GetUserById([FromRoute] Guid id)
        {
            var user = await _userService.GetByIdAsync(id);
            return Ok(user);
        }
        /// <summary>
        /// Updates the user.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <param name="model">The user model.</param>
        /// <returns></returns>
        [HttpPut("{id}")]
        [ServiceFilter(typeof(AccessFilter))]
        [ServiceFilter(typeof(UserModelValidationFilter))]
        public async Task<ActionResult> UpdateUser([FromRoute] Guid id, [FromBody] UserModel model)
        {
            await _userService.UpdateAsync(model);
            return Ok();
        }
        /// <summary>
        /// Deteles the user.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <returns></returns>
        [HttpDelete("{id}")]
        [Authorize(Roles = "Administrator")]
        public async Task<ActionResult> DeteleUser([FromRoute] Guid id)
        {
            await _userService.DeleteAsync(id);
            return Ok();
        }
        /// <summary>
        /// Gets the reaction by user identifier.
        /// </summary>
        /// <param name="id">The user identifier.</param>
        /// <returns></returns>
        [HttpGet("reactions")]
        [ServiceFilter(typeof(AccessFilter))]
        public async Task<ActionResult<IEnumerable<ReactionModel>>> GetReactionByUserId()
        {
            var userId = Guid.Parse(Request.HttpContext.User.Identity.Name);

            var reactions = await _userService.GetReactionByUserIdAsync(userId);
            return Ok(reactions);
        }
    }
}