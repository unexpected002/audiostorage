﻿using BLL.Exceptions;
using BLL.Interfaces;
using BLL.Models.Audio;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PL.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PL.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize(Roles = "User, Administrator")]
    public class OverviewController : ControllerBase
    {
        private readonly IStatisticService _statisticService;

        public OverviewController(IStatisticService statisticService)
        {
            _statisticService = statisticService;
        }
        /// <summary>
        /// Gets the popular audio.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("{id}")]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<PresentationAudioModel>>> GetPopularAudio([FromRoute] Guid id)
        {
            var audio = await _statisticService.GetGenresMostPopularAudioAsync(id);
            return Ok(audio);
        }
        /// <summary>
        /// Gets the puglished audio.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("published-audio")]
        public async Task<ActionResult<IEnumerable<PresentationAudioModel>>> GetPuglishedAudio()
        {
            var userId = Guid.Parse(Request.HttpContext.User.Identity.Name);

            var audio = await _statisticService.GetPostedAudioAsync(userId);
            return Ok(audio);
        }
        /// <summary>
        /// Gets the liked audio.
        /// </summary>
        /// <param name="id">The identifier.</param>
        /// <returns></returns>
        [HttpGet("liked-audio")]
        public async Task<ActionResult<IEnumerable<PresentationAudioModel>>> GetLikedAudio()
        {
            var userId = Guid.Parse(Request.HttpContext.User.Identity.Name);

            var audio = await _statisticService.GetLikedAudioAsync(userId);
            return Ok(audio);
        }
    }
}