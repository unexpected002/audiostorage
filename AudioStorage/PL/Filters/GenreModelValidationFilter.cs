﻿using BLL.Exceptions;
using BLL.Models;
using BLL.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace PL.Filters
{
    public class GenreModelValidationFilter : IActionFilter
    {
        private readonly GenericValidator<GenreModel> _audioModelValidator;

        public GenreModelValidationFilter(GenericValidator<GenreModel> audioModelValidator)
        {
            _audioModelValidator = audioModelValidator;
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if (context.ActionArguments.ContainsKey("model"))
            {
                try
                {
                    _audioModelValidator.Validate((GenreModel)context.ActionArguments["model"]);
                }
                catch (ModelStateException ex)
                {
                    context.Result = new ObjectResult(context.ModelState)
                    {
                        Value = ex.Errors,
                        StatusCode = StatusCodes.Status422UnprocessableEntity
                    };
                }
                catch (ArgumentNullException ex)
                {
                    context.Result = new ObjectResult(context.ModelState)
                    {
                        Value = "Invalid input parameters",
                        StatusCode = StatusCodes.Status406NotAcceptable
                    };
                }
            }
        }
    }
}