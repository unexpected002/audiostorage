﻿using BLL.Exceptions;
using BLL.Models.User;
using BLL.Validation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;

namespace PL.Filters
{
    public class UserModelValidationFilter : IActionFilter
    {
        private readonly GenericValidator<AuthenticateModel> _authenticateModelValidator;
        private readonly GenericValidator<IUserWithGeneralModel> _userShortModelValidator;
        private readonly GenericValidator<RegistrationModel> _userModelValidator;
        private readonly GenericValidator<IUserWithPasswordModel> _userNewPasswordModelValidator;

        public UserModelValidationFilter(GenericValidator<AuthenticateModel> authenticateModelValidator, 
            GenericValidator<IUserWithGeneralModel> userShortModelValidator, GenericValidator<RegistrationModel> userModelValidator,
            GenericValidator<IUserWithPasswordModel> userNewPasswordModelValidator)
        {
            _authenticateModelValidator = authenticateModelValidator;
            _userShortModelValidator = userShortModelValidator;
            _userModelValidator = userModelValidator;
            _userNewPasswordModelValidator = userNewPasswordModelValidator;
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            if(context.ActionArguments.ContainsKey("model"))
            {
                try
                {
                    Type type = context.ActionArguments["model"].GetType();
                    switch(type.Name)
                    {
                        case nameof(AuthenticateModel):
                            {
                                _authenticateModelValidator.Validate((AuthenticateModel)context.ActionArguments["model"]);
                                break;
                            }
                        case nameof(UserModel):
                            {
                                _userShortModelValidator.Validate((UserModel)context.ActionArguments["model"]);
                                break;
                            }
                        case nameof(RegistrationModel):
                            {
                                _userModelValidator.Validate((RegistrationModel)context.ActionArguments["model"]);
                                break;
                            }
                        case nameof(NewPasswordModel):
                            {
                                _userNewPasswordModelValidator.Validate((NewPasswordModel)context.ActionArguments["model"]);
                                break;
                            }
                    }
                }
                catch (ModelStateException ex)
                {
                    context.Result = new ObjectResult(context.ModelState)
                    {
                        Value = ex.Errors,
                        StatusCode = StatusCodes.Status422UnprocessableEntity
                    };
                }
                catch(ArgumentNullException ex)
                {
                    context.Result = new ObjectResult(context.ModelState)
                    {
                        Value = "Invalid input parameters",
                        StatusCode = StatusCodes.Status406NotAcceptable
                    };
                }
            }
        }
    }
}