﻿using BLL.Exceptions;
using BLL.Helpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using System;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Net;
using System.Text;

namespace PL.Filters
{
    public class AccessFilter: IActionFilter
    {
        private readonly AppSettings _appSettings;
        public AccessFilter(IOptions<AppSettings> appSettings)
        {
            _appSettings = appSettings.Value;
        }
        public void OnActionExecuted(ActionExecutedContext context)
        {
        }

        public void OnActionExecuting(ActionExecutingContext context)
        {
            Guid userId = Guid.Empty;
            if (context.ActionArguments.ContainsKey("id"))
            {
                userId = (Guid)context.ActionArguments["id"];
            }
            var authorizedUser = context.HttpContext.User;
            if (authorizedUser != null)
            {
                try
                {
                    Guid authorizeUserId = Guid.Parse(authorizedUser.Identity.Name);
                    if(userId != authorizeUserId && !authorizedUser.IsInRole("Administrator"))
                    {
                        context.Result = new StatusCodeResult(403);
                    }
                }
                catch (Exception ex)
                {
                    context.Result = new StatusCodeResult(404);
                }
            }
        }
    }
}
